//#include <GL/glut.h>
#include <math.h>

#include "igvCamara.h"

// Metodos constructores

igvCamara::igvCamara () {}

igvCamara::~igvCamara () {}

igvCamara::igvCamara(tipoCamara _tipo, igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V) {
	P0 = _P0;
	r = _r;
	V = _V;
	tipo = _tipo;
}


// Metodos publicos 
void igvCamara::set(igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V) {
	P0 = _P0;
	r = _r;
	V = _V;
}
void igvCamara::set(tipoCamara _tipo, igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V,
			                                double _xwmin, double _xwmax, double _ywmin, double _ywmax, double _znear, double _zfar) {
	tipo = _tipo;

	P0 = _P0;
	r = _r;
	V = _V;

	xwmin = _xwmin;
	xwmax = _xwmax;
	ywmin = _ywmin;
	ywmax = _ywmax;
	znear = _znear;
	zfar = _zfar;

	zoomAcumulado = 0;
	subirY = bajarY = true;

	Pcoche[0] = Pcoche[1] = Pcoche[2] = 0;

	cambioznear = 0;
}

void igvCamara::setCoche(igvPunto3D _Pco) {

	Pcoche = _Pco;
}

void igvCamara::set(tipoCamara _tipo, igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V,
			                         double _angulo, double _raspecto, double _znear, double _zfar) {
	tipo = _tipo;

	P0 = _P0;
	r = _r;
	V = _V;

	angulo = _angulo;
	raspecto = _raspecto;
	znear = _znear;
	zfar = _zfar;
}

void igvCamara::aplicar(void) {

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (tipo == IGV_PARALELA) {
		znear = -10;
		glOrtho(xwmin, xwmax, ywmin, ywmax, znear+cambioznear, zfar);
	}
	if (tipo == IGV_FRUSTUM) {
		glFrustum(xwmin, xwmax, ywmin, ywmax, znear+cambioznear, zfar);
	}
	if (tipo == IGV_PERSPECTIVA) {
		znear = 1;
		gluPerspective(angulo, raspecto, znear, zfar);
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (!modoCoche){
		gluLookAt(P0[X], P0[Y], P0[Z], r[X], r[Y], r[Z], V[X], V[Y], V[Z]);
	}
	else {
		gluLookAt(Pcoche[X], Pcoche[Y], Pcoche[Z], r[X], r[Y], r[Z], V[X], V[Y], V[Z]);
	}
}

void igvCamara::zoom(double factor) {
	xwmin *= factor;
	xwmax *= factor;
	ywmin *= factor;
	ywmax *= factor;

	angulo *= factor;

	aplicar();
}

void igvCamara::movimientoCamara(int sentido) {
	switch (sentido) {
	case 1:
		movimientoAcumulado++;
		break;
	case 2:
		movimientoAcumulado--;
		break;
	case 3:
		if (subirY) {
			movimientoAcumulado2++;
		}
		bajarY = true;
		break;
	case 4:
		if (bajarY) {
			movimientoAcumulado2--;
		}
		subirY = true;
		break;
	}

	float alpha = 360.0 / SALTO_MOVIMIENTO_CAMARA;
	float anguloRad = (movimientoAcumulado * alpha) * (M_PI / 180);
	float anguloRad2 = (movimientoAcumulado2 * alpha) * (M_PI / 180);


	int k = 0;
	if (tipo == IGV_PERSPECTIVA) {
		P0[k++] = RADIO * cos(anguloRad) * factorMultPerspectiva;
		P0[k++] = RADIO * sin(anguloRad2);
		P0[k++] = RADIO * sin(anguloRad) * factorMultPerspectiva;
	}
	else {
		P0[k++] = RADIO * cos(anguloRad);
		P0[k++] = RADIO * sin(anguloRad2);
		P0[k++] = RADIO * sin(anguloRad);
	}
	

	if (movimientoAcumulado == SALTO_MOVIMIENTO_CAMARA) {
		movimientoAcumulado = 0;
	}
	else if (movimientoAcumulado == 0) {
		movimientoAcumulado = SALTO_MOVIMIENTO_CAMARA;
	}
	if ((int)round(P0[1]) == (int)round(RADIO)) {
		subirY = false;
	}
	else if ((int)round(P0[1]) == (int)round(-RADIO)) {
		bajarY = false;
	}


	//printf("x: %.2f, y: %.2f, z: %.2f \n",P0[0],P0[1],P0[2]);
	

}

