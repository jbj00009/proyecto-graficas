#include <cstdlib>
#include <stdio.h>
#include <math.h>
#include "igvMallaTriangulos.h"

igvMallaTriangulos::igvMallaTriangulos() :num_vertices(0), vertices(nullptr), num_triangulos(0), triangulos(nullptr),normales(nullptr)
{
}

igvMallaTriangulos::igvMallaTriangulos(long int _num_vertices, float* _vertices, long int _num_triangulos, unsigned int* _triangulos,int tipo) {
	this->tipo = tipo;

	color = new float[3];
	colorQueSeVe = new float[3];
	colorQueSeVeRespaldo = new float[3];

	num_vertices = _num_vertices;
	vertices = new float[num_vertices * 3];
	for (long int i = 0; i < (num_vertices * 3); ++i) {
		vertices[i] = _vertices[i];
	}

	normales = nullptr;

	normales2 = nullptr;

	num_triangulos = _num_triangulos;
	triangulos = new unsigned int[num_triangulos * 3];
	for (long int i = 0; i < (num_triangulos * 3); ++i) {
		triangulos[i] = _triangulos[i];
	}

	//direccion = gradosRotacion = 0;
	difX = difZ = 0;
	origX = origZ = 0;
	
	for (int i = 0; i < 3; i++) {
		color[i] = colorQueSeVe[i] = colorQueSeVeRespaldo[i] = 0.0;
	}
}

igvMallaTriangulos::~igvMallaTriangulos() {
	if (vertices)
		delete []vertices;
	if (normales)
		delete []normales;
	if (triangulos)
		delete []triangulos;
	if (normales2)
		delete[]normales2;

	delete color;
	delete colorQueSeVe;
	delete colorQueSeVeRespaldo;
}



void igvMallaTriangulos::visualizar(void) {

	//if (seleccionada) {
	//	colorQueSeVe[0] = 0;
	//	colorQueSeVe[1] = 1;
	//	colorQueSeVe[2] = 0;
	//}
	//else {
	//	colorQueSeVe[0] = colorQueSeVeRespaldo[0];
	//	colorQueSeVe[1] = colorQueSeVeRespaldo[1];
	//	colorQueSeVe[2] = colorQueSeVeRespaldo[2];
	//}
	//
	if (tipo == 0 || tipo == 3) {
		if (!smooth) {
			glShadeModel(GL_FLAT);
		}
		else {
			glShadeModel(GL_SMOOTH);
		}

		if (!verNormales && !verNormales2) {
			glEnableClientState(GL_VERTEX_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, vertices);

			glDrawElements(GL_TRIANGLES, num_triangulos * 3, GL_UNSIGNED_INT, triangulos);

			glDisableClientState(GL_VERTEX_ARRAY);
		}
		else if (verNormales) {
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_NORMAL_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, vertices);
			glNormalPointer(GL_FLOAT, 0, normales);

			glDrawElements(GL_TRIANGLES, num_triangulos * 3, GL_UNSIGNED_INT, triangulos);

			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_NORMAL_ARRAY);
		}
		else {
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_NORMAL_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, vertices);
			glNormalPointer(GL_FLOAT, 0, normales2);

			glDrawElements(GL_TRIANGLES, num_triangulos * 3, GL_UNSIGNED_INT, triangulos);

			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_NORMAL_ARRAY);
		}
	}

	/* Apartado B: TODO */

}

GLubyte* igvMallaTriangulos::getColorByte() {
	GLubyte colorubyte[3];
	colorubyte[0] = (GLubyte)(color[0] * 255);
	colorubyte[1] = (GLubyte)(color[1] * 255);
	colorubyte[2] = (GLubyte)(color[2] * 255);

	return colorubyte;
}

void igvMallaTriangulos::setColorAlgoritmo(float r, float g, float b) {
	color[0] = r;
	color[1] = g;
	color[2] = b;
}

void igvMallaTriangulos::setColor(float r, float g, float b) {
	colorQueSeVe[0] = r;
	colorQueSeVe[1] = g;
	colorQueSeVe[2] = b;
}

void igvMallaTriangulos::setColorRespaldo(float r, float g, float b) {
	colorQueSeVeRespaldo[0] = r;
	colorQueSeVeRespaldo[1] = g;
	colorQueSeVeRespaldo[2] = b;
}


void igvMallaTriangulos::setSeleccionar(bool estado) {
	seleccionada = estado;
}

