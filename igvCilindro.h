
#include "igvMallaTriangulos.h"
#include <math.h>
#define _USE_MATH_DEFINES
# define M_PI           3.14159265358979323846

class igvCilindro : public igvMallaTriangulos {


public:

	//// Constructores por y destructor
	igvCilindro();
	igvCilindro(float r, float a, int divU, int divV, int tipo);
	~igvCilindro();

	//funciones propias
	void imprimirInfo(float r, float a, int divU, int divV);
};

