#include <cstdlib>
#include <stdio.h>
#include <cmath>
#include <iostream>
#include "igvEscena3D.h"
#include <iomanip>
#include "igvMallaTriangulos.h"
#include "igvFuenteLuz.h"
#include "igvMaterial.h"
#include "igvTextura.h"


// Metodos constructores 

void pintar_pared1(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 20.0;
	float fin_z = 5.425;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-10, -10, -4.4);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_Estanteria(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 5.1;
	float fin_z = 4.6;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-2.55, -6.2, -3.6);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_pared2(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 20.0;
	float fin_z = 5.425;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-10, 1.685, 0);
	glRotated(180, 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(90, 0, 0, 1);
	glTranslated(-fin_x / 2, 0, -fin_z / 2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_quad(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 20.0;
	float fin_z = 20.0;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-10, -0.95, -10);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}
void pintar_quad2(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 6.425;
	float fin_z = 4.225;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-fin_x/2, 0.41, -fin_z/2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_quad3(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 6.425;
	float fin_z = 0.425;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(0,0.2,2.11);
	glRotated(90, 1, 0, 0);
	glTranslated(-fin_x / 2, 0, -fin_z / 2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_quad4(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 6.425;
	float fin_z = 4.225;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-fin_x / 2, -0.01, -fin_z / 2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_quad5(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 4.225;
	float fin_z = 0.425;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(3.21, 0.2, 0);
	glRotated(90, 1, 0, 0);
	glRotated(90, 0, 0, 1);
	glTranslated(-fin_x / 2, 0, -fin_z / 2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_quad6(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 4.225;
	float fin_z = 0.425;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(-3.21, 0.2, 0);
	glRotated(90, 1, 0, 0);
	glRotated(90, 0, 0, 1);
	glTranslated(-fin_x / 2, 0, -fin_z / 2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();

}

void pintar_quad7(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float fin_x = 6.425;
	float fin_z = 0.425;

	float partes_x = fin_x / div_x;
	float partes_z = fin_z / div_z;

	float x = partes_x;
	float z = partes_z;


	glNormal3f(0, 1, 0);
	glPushMatrix();
	glTranslated(0, 0.2, -2.11);
	glRotated(90, 1, 0, 0);
	glTranslated(-fin_x / 2, 0, -fin_z / 2);
	for (float i = 0; i < div_z; ++i) {
		for (int j = 0; j < div_x; ++j) {
			glBegin(GL_QUADS);
			glTexCoord2f(ini_x / fin_x, ini_z / fin_z);
			glVertex3f(ini_x, 0.0, ini_z);

			glTexCoord2f(ini_x / fin_x, z / fin_z);
			glVertex3f(ini_x, 0.0, z);

			glTexCoord2f(x / fin_x, z / fin_z);
			glVertex3f(x, 0.0, z);

			glTexCoord2f(x / fin_x, ini_z / fin_z);
			glVertex3f(x, 0.0, ini_z);
			glEnd();

			ini_x += partes_x;
			x += partes_x;
		}
		ini_x = 0.0;
		x = partes_x;
		ini_z += partes_z;
		z += partes_z;
	}
	glPopMatrix();
}
igvEscena3D::igvEscena3D() {
	ejes = true;
	anguloX = anguloY = anguloZ = 0;
	movimientoX = movimientoY = movimientoZ=0;
	difX = difY = difZ = 0;
	trasladaX = trasladaZ = 0;

	dibujarTexto = true;

	algoritmoGenerarObjetos();
	algoritmoGenerarColores();
	algoritmoPintarObjetos();
	
	pixelSeleccionado = new GLfloat[3];

	//variables coche
	factorGiro = 0;
	rotaRuedaY = rotaRuedaZ = 0;
	animacion = false;
	anguloParabrisas = 90;
	aperturaVentana = 0.6;
	giraY = 0;
	posXcoche = 3;
	posYcoche= 1;

	//variables figura
	giraCabeza = 0;
	sentidoCabeza = false;
}

igvEscena3D::~igvEscena3D() {
}


// Metodos publicos 

void pintar_ejes(void) {
	GLfloat rojo[] = { 1,0,0,1.0 };
	GLfloat verde[] = { 0,1,0,1.0 };
	GLfloat azul[] = { 0,0,1,1.0 };

	glMaterialfv(GL_FRONT, GL_EMISSION, rojo);
	glBegin(GL_LINES);
	glVertex3f(1000, 0, 0);
	glVertex3f(-1000, 0, 0);
	glEnd();

	glMaterialfv(GL_FRONT, GL_EMISSION, verde);
	glBegin(GL_LINES);
	glVertex3f(0, 1000, 0);
	glVertex3f(0, -1000, 0);
	glEnd();

	glMaterialfv(GL_FRONT, GL_EMISSION, azul);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 1000);
	glVertex3f(0, 0, -1000);
	glEnd();
}



void igvEscena3D::pintar_tubo() {
	GLUquadricObj* tubo;
	GLfloat color_tubo[] = { 0,0,0.5 };

	glMaterialfv(GL_FRONT, GL_EMISSION, color_tubo);

	tubo = gluNewQuadric();
	gluQuadricDrawStyle(tubo, GLU_FILL);

	glPushMatrix();
	glTranslatef(0, 0, -0.5);
	gluCylinder(tubo, 0.25, 0.25, 1, 20, 20);
	glPopMatrix();

	gluDeleteQuadric(tubo);
}

void igvEscena3D::aumentaX() {
	movX += 0.2;
}

void igvEscena3D::disminuyeX() {
	movX -= 0.2;
}

void igvEscena3D::algoritmoGenerarColores() {
	vColores = new float[vectorMalla.size() * 3];

	incrementoParticiones = (256 / vectorMalla.size()) * 2;

	bool activaByte2 = false;
	bool activaByte3 = false;
	float c1, c2, c3;
	c1 = c2 = c3 = 0.0;
	for (int i = 0; i < (vectorMalla.size()) * 3; i++) {
		vColores[i] = 0.0;
	}

	for (int i = 0; i < (vectorMalla.size()) * 3; i += 3) {
		if (!activaByte2 && !activaByte3) {
			vColores[i] = c1 + incrementoParticiones;
			c1 = vColores[i];
		}

		if (activaByte2) {
			vColores[i + 1] = c2 + incrementoParticiones;
			c2 = vColores[i + 1];
		}
		if (activaByte3) {
			vColores[i + 2] = c3 + incrementoParticiones;
			c3 = vColores[i + 2];
		}

		if (c1 + incrementoParticiones >= 255) {
			activaByte2 = true;
			c1 = 0;
		}
		if (c2 + incrementoParticiones >= 255) {
			activaByte3 = true;
			c1 = 0;
			c2 = 0;
		}
	}
	/*for (int i = 0; i < vectorMalla.size() * 3; i += 3) {
		printf("%.1f , %.1f, %.1f \n", vColores[i], vColores[i + 1], vColores[i + 2]);
	}*/
}

void igvEscena3D::algoritmoGenerarObjetos() {
	vectorMalla.clear();

	vectorMalla.push_back(new igvCilindro(0.1, 1, 20, 3, 0));
	vectorMalla.push_back(new igvCuadrado(-1, 1, 0.4, 16, 2, 0));
	vectorMalla.push_back(new igvCilindro(1, 0.2, 4, 3, 0));
	vectorMalla.push_back(new igvCuadrado(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvMallaTriangulos(0,0,0,0,1));
	vectorMalla.push_back(new igvCuadrado(-1, 1, 1, 16, 2, 3));
	vectorMalla.push_back(new igvCuadrado(-1, 1, 1, 16, 2, 0));//suelo verde

	//mallas coche
	vectorMalla.push_back(new igvTriangulo(-1, 1,0, 16, 2,0));
	vectorMalla.push_back(new igvCuadrado(-1, 1, 1, 16, 2,0));
	vectorMalla.push_back(new igvCilindro(1, 1, 40, 3,0));

	vectorMalla.push_back(new igvCuadrado(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvTriangulo(-1, 1, 1, 16, 2, 0));
	vectorMalla.push_back(new igvMallaTriangulos(0, 0, 0, 0, 1));
	vectorMalla.push_back(new igvCuadrado(-1, 1, 1, 16, 2, 3));

	//nube
	vectorMalla.push_back(new igvCilindro(1, 0.5, 4, 3, 0));
}

void igvEscena3D::algoritmoPintarObjetos() {
	int cont = 0;
	for (int i = 0; i < vectorMalla.size(); i++) {
		vectorMalla[i]->setColorAlgoritmo(vColores[cont]/255, vColores[cont + 1]/255, vColores[cont + 2]/255);
		cont += 3;
	}

	vectorMalla[0]->setColor(0.5, 0.25, 0);
	vectorMalla[0]->setColorRespaldo(0.5, 0.25, 0);

	vectorMalla[1]->setColor(convierteDeRgb(65), convierteDeRgb(35), convierteDeRgb(7));
	vectorMalla[1]->setColorRespaldo(convierteDeRgb(65), convierteDeRgb(35), convierteDeRgb(7));

	vectorMalla[2]->setColor(0, 0, 0);
	vectorMalla[2]->setColorRespaldo(0, 0, 0);

	vectorMalla[3]->setColor(convierteDeRgb(80), convierteDeRgb(148), convierteDeRgb(0));
	vectorMalla[3]->setColorRespaldo(convierteDeRgb(80), convierteDeRgb(148), convierteDeRgb(0));

	vectorMalla[4]->setColor(convierteDeRgb(81), convierteDeRgb(0), convierteDeRgb(102));
	vectorMalla[4]->setColorRespaldo(convierteDeRgb(81), convierteDeRgb(0), convierteDeRgb(102));

	vectorMalla[5]->setColor(0, 0, 1);
	vectorMalla[5]->setColorRespaldo(0, 0, 1);

	vectorMalla[6]->setColor(convierteDeRgb(255), convierteDeRgb(93), convierteDeRgb(247));
	vectorMalla[6]->setColorRespaldo(convierteDeRgb(255), convierteDeRgb(93), convierteDeRgb(247));

	vectorMalla[7]->setColor(convierteDeRgb(221), convierteDeRgb(161), 0);
	vectorMalla[7]->setColorRespaldo(convierteDeRgb(221), convierteDeRgb(161), 0);

	vectorMalla[8]->setColor(1, 0, 0);
	vectorMalla[8]->setColorRespaldo(1, 0, 0);

	vectorMalla[9]->setColor(1, 1, 0);
	vectorMalla[9]->setColorRespaldo(1, 1, 0);

	vectorMalla[10]->setColor(0, 0.5, 0.5);
	vectorMalla[10]->setColorRespaldo(convierteDeRgb(167),convierteDeRgb(168),0);

	vectorMalla[11]->setColor(convierteDeRgb(124), convierteDeRgb(186), convierteDeRgb(37));
	vectorMalla[11]->setColorRespaldo(convierteDeRgb(124), convierteDeRgb(186), convierteDeRgb(37));

	vectorMalla[15]->setColor(convierteDeRgb(80), convierteDeRgb(148), convierteDeRgb(0));
	vectorMalla[15]->setColorRespaldo(convierteDeRgb(80), convierteDeRgb(148), convierteDeRgb(0));

	vectorMalla[16]->setColor(convierteDeRgb(81), convierteDeRgb(0), convierteDeRgb(102));
	vectorMalla[16]->setColorRespaldo(convierteDeRgb(81), convierteDeRgb(0), convierteDeRgb(102));

	vectorMalla[17]->setColor(0, 0, 1);
	vectorMalla[17]->setColorRespaldo(0, 0, 1);

	vectorMalla[18]->setColor(convierteDeRgb(255), convierteDeRgb(93), convierteDeRgb(247));
	vectorMalla[18]->setColorRespaldo(convierteDeRgb(255), convierteDeRgb(93), convierteDeRgb(247));

	vectorMalla[19]->setColor(convierteDeRgb(221), convierteDeRgb(161), 0);
	vectorMalla[19]->setColorRespaldo(convierteDeRgb(221), convierteDeRgb(161), 0);

	vectorMalla[20]->setColor(1, 0, 0);
	vectorMalla[20]->setColorRespaldo(1, 0, 0);

	/*vectorMalla[21]->setColor(1, 0, 0);
	vectorMalla[21]->setColorRespaldo(1, 0, 0);

	vectorMalla[22]->setColor(0, 0.5, 0.5);
	vectorMalla[22]->setColorRespaldo(convierteDeRgb(167), convierteDeRgb(168), 0);*/

	vectorMalla[23]->setColor(0, 0, 1);
	vectorMalla[23]->setColorRespaldo(0, 0, 1);
}


void igvEscena3D::visualizar(void) {
	// crear luces
	//GLfloat luz0[4] = { 0,0,1,0 }; // luz puntual para visualizar el cubo
	GLfloat luz0[4] = { 10.0,10.0,10.0,1 }; // luz puntual para visualizar el cubo

	
	glLightfv(GL_LIGHT0, GL_POSITION, luz0); // la luz se coloca aqu� si permanece fija y no se mueve con la escena
	glEnable(GL_LIGHT0);
	/*glMatrixMode(GL_MODELVIEW);*/
	
	glPushMatrix();
		if (ejes) pintar_ejes();
	glPopMatrix(); // restaura la matriz de modelado

	igvMaterial defaultMat(igvColor(0.2, 0.2, 0.2, 1), igvColor(0.8, 0.8, 0.8, 1), igvColor(0.0, 0, 0, 1), 0);
	defaultMat.aplicar();
	glPushMatrix();
		visualizarVB();
	glPopMatrix();

	if (dibujarTexto) {
		std::string str = std::to_string(FPS);
		string str2 = "FPS: " + str;
		char* cstr = new char[str2.length() + 1];
		strcpy(cstr, str2.c_str());
		dibujaTexto(0, 5, 0, cstr);
		delete[] cstr;
	}
	
}

void igvEscena3D::visualizarVB(void) {
	GLfloat color_malla[] = { 0,0.25,0 };
	GLfloat color_malla2[] = { 0.5,0.25,0 };
	GLfloat color_marron[] = { convierteDeRgb(65),convierteDeRgb(35),convierteDeRgb(7) };//marron
	GLfloat color_negro[] = { 0,0,0 };
	GLfloat color_morado[] = { convierteDeRgb(81),convierteDeRgb(0),convierteDeRgb(102) };//morado
	GLfloat color_amarillo[] = { 1,1,0 };//amarillo
	GLfloat color_rojo[] = { 1,0,0 };
	GLfloat color_verde[] = { convierteDeRgb(80),convierteDeRgb(148),convierteDeRgb(0) };//verde
	GLfloat color_rosa[] = { convierteDeRgb(255),convierteDeRgb(93),convierteDeRgb(247) };//rosa
	GLfloat color_naranja[] = { convierteDeRgb(221),convierteDeRgb(161),0 };
	GLfloat color_azul[] = { 0,0,1 };
	GLfloat seleccionado[] = { 1,0,1 };

	//GLfloat luz0[4] = { 2,2,5.0,1 }; // luz puntual
	//glLightfv(GL_LIGHT0, GL_POSITION, luz0); // la luz se coloca aqu� si se mueve junto con la escena

	//igvFuenteLuz luz = igvFuenteLuz(GL_LIGHT0, igvPunto3D(-1, 3, -1), igvColor(0.0, 0.0, 0.0, 1.0), igvColor(0.5, 0.5, 0.5, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), 1, 0, 0);
	//luz.aplicar();


	//igvFuenteLuz luz2 = igvFuenteLuz(GL_LIGHT1, { 3,0,1 }, { 0,0,0,1 }, { 1,1,1,1 }, { 1,1,1,1 }, 1, 0, 0, { 0, -1, 0 }, 40, 1);//penultimo angulo, ultimo intensidad inv prop
	//luz2.aplicar();

	//double ns = 120;
	//igvMaterial material = igvMaterial({ convierteDeRgb(124),0,0 }, { convierteDeRgb(186),0,0 }, { convierteDeRgb(37) ,0 , 0 }, ns);

	//material.aplicar();

	//glLightfv(GL_LIGHT0,GL_POSITION,luz0); // la luz se coloca aqu� si se mueve junto con la escena
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_malla2);
	//Transformaciones a todos los objetos
	glRotated(anguloX, 1, 0, 0);
	glRotated(anguloY, 0, 1, 0);
	glRotated(anguloZ, 0, 0, 1);
	
	glTranslated(movimientoX, movimientoY, movimientoZ);

	//printf("%.2f , %.2f, %.2f \n", vectorMalla[0]->getColorAlgoritmo()[0], vectorMalla[0]->getColorAlgoritmo()[1], vectorMalla[0]->getColorAlgoritmo()[2]);

	/*igvMaterial material = igvMaterial(igvColor(0.15, 0.0, 0.0), igvColor(0.5, 0.0, 0.0), igvColor(0.5, 0.0, 0.0), 120);
	material.aplicar();*/

	//SUELO
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[11]->getColorQueSeVe());
	glColor3fv(vectorMalla[11]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(0,-1.2,0);
	glScaled(10,0.2,10);
	vectorMalla[11]->visualizar();
	glPopMatrix();

	//PATAS DE LA MESA
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[0]->getColorQueSeVe());
	glColor3fv(vectorMalla[0]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(2*-1.5, -1, 2);
	vectorMalla[0]->visualizar();
	glPopMatrix();

	glPushMatrix();
	glTranslated(2*1.5, -1, 2);
	vectorMalla[0]->visualizar();
	glPopMatrix();

	glPushMatrix();
	glTranslated(2*-1.5, -1, -2);
	vectorMalla[0]->visualizar();
	glPopMatrix();

	glPushMatrix();
	glTranslated(2*1.5, -1, -2);
	vectorMalla[0]->visualizar();
	glPopMatrix();

	//MESA
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_marron);
	/*igvMaterial defaultMat(igvColor(0.2, 0.2, 0.2, 1), igvColor(0.8, 0.8, 0.8, 1), igvColor(0.0, 0, 0, 1), 0);
	defaultMat.aplicar();*/
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[1]->getColorQueSeVe());
	glColor3fv(vectorMalla[1]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glScaled(3.2, 1, 2.1);
	vectorMalla[1]->visualizar();
	glPopMatrix();

	int factorConversion = 1 / sqrt(2);//el cuadrado de figuras mide raiz de 2 y quiero que mida 1. sqrt(2)=*x=1--> x=1/sqrt(2)

	//Soporte figuritas (mide 2 raiz de 2 unidades de ancho y largo)

	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[2]->getColorQueSeVe());
	glColor3fv(vectorMalla[2]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_negro);
	glPushMatrix();
	glTranslated(0, 0.4, 0);
	glScaled(2.01, 1.01, 2.01);
	glRotated(45, 0, 1, 0);
	//vectorMalla[2]->visualizar();
	glPopMatrix();

	//figuritas
	//rombo
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_verde);
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[3]->getColorQueSeVe());
	glColor3fv(vectorMalla[3]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(0, 0.4, 0);
	glTranslated(0, 0, sqrt(2) / 2);
	glTranslated(vectorMalla[3]->getdifX(), 0, vectorMalla[3]->getdifZ());
	glRotated(vectorMalla[3]->getRotacion(), 0, 1, 0);
	glRotated(45, 0, 1, 0);
	glScaled(0.5, 0.2, 0.5);
	vectorMalla[3]->visualizar();
	glPopMatrix();

	//Triangulos
	//triangulo mitad
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_morado);
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[4]->getColorQueSeVe());
	glColor3fv(vectorMalla[4]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(0, 0.4, -0.7);
	glTranslated(movX, 0.2, sqrt(2) / 2);
	glTranslated(vectorMalla[4]->getdifX(), 0, vectorMalla[4]->getdifZ());
	glScaled(1, 0.2, 1);
	glRotated(vectorMalla[4]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(-45, 0, 0, 1);
	vectorMalla[4]->visualizar();
	glPopMatrix();

	
	
	//Segundo Triangulo (abajo izq)
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_rosa);
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[5]->getColorQueSeVe());
	glColor3fv(vectorMalla[5]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(-0.7, 0.4, 0);
	glTranslated(0, 0.2, sqrt(2) / 2);
	glTranslated(vectorMalla[5]->getdifX(), 0, vectorMalla[5]->getdifZ());
	glScaled(1, 0.2, 1);
	glRotated(vectorMalla[5]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(45, 0, 0, 1);
	vectorMalla[5]->visualizar();
	glPopMatrix();

	//Tercer Triangulo (izquierda)
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_azul);
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[6]->getColorQueSeVe());
	glColor3fv(vectorMalla[6]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(0, 0.4, -0.7);
	glTranslated(0, 0.2, sqrt(2) / 2);
	glTranslated(vectorMalla[6]->getdifX(), 0, vectorMalla[6]->getdifZ());
	glScaled(2, 0.2, 2);
	glRotated(vectorMalla[6]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(135, 0, 0, 1);
	vectorMalla[6]->visualizar();
	glPopMatrix();

	//Cuarto Tri�ngulo(arriba)
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_naranja);
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[7]->getColorQueSeVe());
	glColor3fv(vectorMalla[7]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(0, 0.4, -0.7);
	glTranslated(0, 0.2, sqrt(2) / 2);
	glTranslated(vectorMalla[7]->getdifX(), 0, vectorMalla[7]->getdifZ());
	glScaled(2, 0.2, 2);
	glRotated(vectorMalla[7]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(-135, 0, 0, 1);
	vectorMalla[7]->visualizar();
	glPopMatrix();

	//printf("%Mira: %f \n", vectorMalla[8]->getorigX());
	//Ultimo triangulo (abajo derecha)
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_rojo);
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[8]->getColorQueSeVe());
	glColor3fv(vectorMalla[8]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(1.4, 0.4, 0.7);
	glTranslated(0, 0.2, sqrt(2) / 2);
	glTranslated(vectorMalla[8]->getdifX(),0, vectorMalla[8]->getdifZ());
	glScaled(1.4, 0.2, 1.4);
	glRotated(vectorMalla[8]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(180, 0, 0, 1);
	vectorMalla[8]->visualizar();
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[10]->getColorQueSeVe());
	glColor3fv(vectorMalla[10]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(0.7, 0.6, 0.7);
	glTranslated(vectorMalla[10]->getdifX(), 0, vectorMalla[10]->getdifZ());
	glRotated(90, 0, 1, 0);
	glRotated(vectorMalla[10]->getRotacion(), 0, 1, 0);
	glScaled(1.4, 1, 1.4);
	vectorMalla[10]->visualizar();
	glPopMatrix();

	//Si el flexo est� seleccionado, se pone la variable flexoSeleccionado a true, y podemos rotar la parte de arriba y mover TODO el flexo con el raton, pero para rotarlo con teclas
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[9]->getColorQueSeVe());
	glColor3fv(vectorMalla[9]->getColorAlgoritmo());//Importante, es necesario para que lo reconozca el buffer de color a la hora de la selecci�n.
	glPushMatrix();
	glTranslated(-1, 0.4, 1.8);
	//glTranslated(mueveFlexoZ, 0,0);
	glTranslated(vectorMalla[9]->getdifX(), 0, vectorMalla[9]->getdifZ());
	glRotated(vectorMalla[9]->getRotacion(), 0, 1, 0);
	glScaled(0.2, 0.2, 0.2);
	vectorMalla[9]->visualizar();
	baseFlexo();
	glPopMatrix();

	

	//GRAFO COCHE
	glPushMatrix();
	glTranslated(posXcoche, -0.82, posYcoche);
	//printf("%.3f, %.3f \n", posXcoche, posYcoche);
	glRotated(180 - anguloOrigen, 0, 1, 0);

	glRotated(anguloX, 1, 0, 0);
	glRotated(anguloY, 0, 1, 0);
	glRotated(anguloZ, 0, 0, 1);

	glTranslated(movimientoX, movimientoY, movimientoZ);
	glScaled(0.25,0.25,0.25);
	coche();
	glPopMatrix();

	glPushMatrix();
	glTranslated(-2, 0, 0);
	gConejo();
	glPopMatrix();

	//TEXTURAS
	glPushMatrix(); // mesa
	igvMaterial material2 = igvMaterial(igvColor(0.25, 0.164706, 0.164706, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), 90);
	material2.aplicar();
	char texture10[] = "madera.jpg";
	igvTextura texturee(texture10);
	texturee.aplicar();
	pintar_quad(150, 150);
	glPopMatrix();

	glPushMatrix();
	igvMaterial material = igvMaterial(igvColor(1.0, 1.0, 1.0, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), 90);
	material.aplicar();

	char texture[] = "textura_mesa.jpg";//+y
	igvTextura textura(texture);
	textura.aplicar();
	pintar_quad2(100, 100);
	glPopMatrix();

	glPushMatrix();//+z
	char texture2[] = "textura_mesa.jpg";
	igvTextura textura2(texture2);
	textura2.aplicar();
	pintar_quad3(50, 50);
	glPopMatrix();


	glPushMatrix();//-y
	char texture3[] = "textura_mesa.jpg";
	igvTextura textura3(texture3);
	textura3.aplicar();
	pintar_quad4(5, 5);
	glPopMatrix();


	glPushMatrix();//+x
	char texture5[] = "textura_mesa.jpg";
	igvTextura textura5(texture5);
	textura5.aplicar();
	pintar_quad5(50, 50);
	glPopMatrix();

	glPushMatrix();//-x
	char texture6[] = "textura_mesa.jpg";
	igvTextura textura6(texture6);
	textura6.aplicar();
	pintar_quad6(10, 10);
	glPopMatrix();

	glPushMatrix();//-z
	char texture7[] = "textura_mesa.jpg";
	igvTextura textura7(texture7);
	textura7.aplicar();
	pintar_quad7(10, 10);
	glPopMatrix();

	glPushMatrix();
	char texture8[] = "pared.jpg";
	igvTextura textura8(texture8);
	textura8.aplicar();
	glRotated(90, 1, 0, 0);
	pintar_pared1(50,50);
	glPopMatrix();

	glPushMatrix();
	pintar_pared2(50, 50);
	glPopMatrix();

	//Estanter�a
	glPushMatrix();
	GLfloat colorEstanter�a[3] = { 0.6039,0.3686,0.1568 };
	glMaterialfv(GL_FRONT, GL_EMISSION, colorEstanter�a);
	glColor3fv(colorEstanter�a);
	igvMaterial materialEst = igvMaterial(igvColor(0.25, 0.164706, 0.164706, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), 90);
	materialEst.aplicar();
	char textureEst[] = "estant_fondo.jpg";
	igvTextura textureE(textureEst);
	textureE.aplicar();
	glPushMatrix();
	glTranslated(0, 1.2, -7);
	glScaled(1.7, 1.6, 0.5);
	glutSolidCube(3);
	glPopMatrix();

	glPushMatrix();
	glRotated(90, 1, 0, 0);
	pintar_Estanteria(25, 25);
	glPopMatrix();

	glRotated(90, 0, 1, 0);
	glMaterialfv(GL_FRONT, GL_EMISSION, colorEstanter�a);
	glColor3fv(colorEstanter�a);
	glPushMatrix();
	glTranslated(0, 1.2, -7);
	glScaled(1.7, 1.6, 0.5);
	glutSolidCube(3);
	glPopMatrix();
	glPopMatrix();

	glRotated(90, 0, 1, 0);
	glPushMatrix();
	glRotated(90, 1, 0, 0);
	pintar_Estanteria(25, 25);
	glPopMatrix();
	glPopMatrix();

}



void igvEscena3D::rotarCabezaConejoAbajo() {
	if (rotaCabezaConejo < 25) {
		rotaCabezaConejo += 0.1;
	}
	else {
		cambioCabezaConejo = true;
	}
}

void igvEscena3D::rotarCabezaConejoArriba() {
	if (rotaCabezaConejo > 0) {
		rotaCabezaConejo -= 0.1;
	}
	else {
		cambioCabezaConejo = false;
	}
}





void igvEscena3D::rotarOreja1ConejoAbajo() {
	if (rotaOreja1Conejo < 220) {
		rotaOreja1Conejo += 0.2;
	}
	else {
		cambioAnimacion = true;
	}
}

void igvEscena3D::rotarOreja1ConejoArriba() {
	if (rotaOreja1Conejo > 180) {
		rotaOreja1Conejo -= 0.2;
	}
}

void igvEscena3D::rotarOreja2ConejoAbajo() {
	if (rotaOreja2Conejo < 0) {
		rotaOreja2Conejo += 0.2;
	}

}

void igvEscena3D::rotarOreja2ConejoArriba() {
	if (rotaOreja2Conejo > -20) {
		rotaOreja2Conejo -= 0.2;
	}
	else {
		cambioAnimacion = false;
	}
}
void igvEscena3D::dibujaPieza18() {
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[19]->getColorQueSeVe());
	glRotated(giraBase, 0, 1, 0);
	glPushMatrix();
	vectorMalla[18]->visualizar();
	glPopMatrix();
}

void igvEscena3D::dibujaPieza18b() {
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[20]->getColorQueSeVe());
	glPushMatrix();
	//glRotated(giraOrejaRoja, 0, 0, 1);
	vectorMalla[18]->visualizar();
	glPopMatrix();
}

void igvEscena3D::dibujaPieza17() {
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[18]->getColorQueSeVe());
	glPushMatrix();
	vectorMalla[17]->visualizar();
	glPopMatrix();
}


void igvEscena3D::dibujaPieza19() {
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[16]->getColorQueSeVe());
	glPushMatrix();
	vectorMalla[19]->visualizar();
	glPopMatrix();
}

void igvEscena3D::dibujaPieza20() {
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[17]->getColorQueSeVe());
	
	glPushMatrix();
	glRotated(giraCabeza2, 0, 0, 1);
	vectorMalla[20]->visualizar();
	glPopMatrix();
}

void igvEscena3D::dibujaPieza15() {
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[15]->getColorQueSeVe());
	glRotated(giraCabeza, 0, 1, 0);
	glPushMatrix();
	vectorMalla[15]->visualizar();
	glPopMatrix();
}

void igvEscena3D::dibujaPieza10() {//oreja azul
	glMaterialfv(GL_FRONT, GL_EMISSION, vectorMalla[10]->getColorQueSeVe());
	glPushMatrix();
	glRotated(giraOrejaAzul,0,1,0);
	vectorMalla[10]->visualizar();
	glPopMatrix();
}

void igvEscena3D::gConejo() {
	glPushMatrix();

	glTranslated(-1, 0, 0);
	/*glTranslated(0, 0.4, -0.7);
	glTranslated(0, 0.2, sqrt(2) / 2);*/
	/*glScaled(1, 0.2, 1);*/
	/*glRotated(vectorMalla[18]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(90, 0, 0, 1);*/
	glTranslated(0, 0.2, 0);
	glTranslated(0, 0.2, sqrt(2) / 2);
	glScaled(0.7, 0.7, 0.7);
	glScaled(1, 1, 0.5);
	dibujaPieza18();
	parteConejoDos();
	glPopMatrix();
}

void igvEscena3D::parteConejoDos() {
	glPushMatrix();
	glTranslated(-1, 0, 0);
	/*glTranslated(0, 0.4, -0.7);
	glTranslated(0, 0.2, sqrt(2) / 2);*/
	/*glScaled(1, 0.2, 1);*/
	/*glRotated(vectorMalla[18]->getRotacion(), 0, 1, 0);
	glRotated(90, 1, 0, 0);
	glRotated(90, 0, 0, 1);*/
	glTranslated(1, 1, 1);
	glRotated(180, 0, 1, 0);
	glRotated(135, 0, 0, 1);
	dibujaPieza17();
	parteConejoTres();
	glPopMatrix();
}

void igvEscena3D::parteConejoTres() {
	glPushMatrix();
	glTranslated(1, 0.5, 1);
	glRotated(180, 0, 1, 0);
	glRotated(-90, 0, 0, 1);
	glScaled(0.5, 0.5, 1);
	dibujaPieza19();
	parteConejoCuatro();
	glPopMatrix();
}

void igvEscena3D::parteConejoCuatro() {
	glPushMatrix();
	glTranslated(0, 1, 1);
	glRotated(-180, 0, 1, 0);
	glRotated(270, 0, 0, 1);
	dibujaPieza20();
	bolaCabezaConejo();
	glPopMatrix();
}

void igvEscena3D::bolaCabezaConejo() {
	glPushMatrix();
	glTranslated(1.7, -1, 0.5);
	glutSolidSphere(0.1, 50, 50);
	parteConejoCinco();
	glPopMatrix();
}

void igvEscena3D::parteConejoCinco() {
	glPushMatrix();
	glTranslated(0.3, -0.3, 0);
	glRotated(rotaCabezaConejo, 0, 0, 1);
	glRotated(45, 0, 0, 1);
	glScaled(0.5, 1, 0.5);
	dibujaPieza15();
	bolaOrejasConejo();
	glPopMatrix();
}

void igvEscena3D::bolaOrejasConejo() {
	glPushMatrix();
	glTranslated(-0.7, 0, 0.5);
	glutSolidSphere(0.1, 50, 50);
	oreja1Conejo();
	oreja2Conejo();
	glPopMatrix();
}

void igvEscena3D::oreja1Conejo() {
	glPushMatrix();
	glRotated(90, 1, 0, 0);
	glRotated(rotaOreja2Conejo, 0, 1, 0);
	glScaled(2, 3, 2);
	dibujaPieza10();
	glPopMatrix();
}

void igvEscena3D::oreja2Conejo() {
	glPushMatrix();
	glScaled(2, 2, 0.5);
	glRotated(rotaOreja1Conejo, 0, 0, 1);
	dibujaPieza18b();
	glPopMatrix();
}

void igvEscena3D::mueveFlexo(GLint x) {
	GLdouble mueve = x;
	GLdouble mueveAux = mueve / 100;
	//std::cout << "mueve es " << mueveAux << std::endl;
	if (mueveAux >= -2 && mueveAux <= 4) {
		mueveFlexoZ = mueveAux;
	}
}

void igvEscena3D::mueveObjeto(int objetoSeleccionado, double trasladaX, double trasladaZ) {
	if (objetoSeleccionado != -1){
		if (objetoSeleccionado == 9) {
			if (trasladaX >= -2 && trasladaX <= 4 && trasladaZ >= -3.5 && trasladaZ <= 0) {
				vectorMalla[objetoSeleccionado]->setdifX(trasladaX);
				vectorMalla[objetoSeleccionado]->setdifZ(trasladaZ);
				//printf("%f, %f \n", trasladaX, trasladaZ);
			}
		}else{
			vectorMalla[objetoSeleccionado]->setdifX(trasladaX);
			vectorMalla[objetoSeleccionado]->setdifZ(trasladaZ);
		}
	}
}

double igvEscena3D::convierteDeRgb(double x) {
	return x / 255;
}

void igvEscena3D::aumentaAngulo(char angulo, int aumento) {
	switch (angulo) {
	case 'X':
		anguloX += aumento;
		break;
	case 'Y':
		anguloY += aumento;
		break;
	case 'Z':
		anguloZ += aumento;
		break;
	}
}

void igvEscena3D::desplazaObjeto(char sentido, double aumento) {
	switch (sentido) {
	case 'X':
		movimientoX += aumento;
		break;
	case 'Y':
		movimientoY += aumento;
		break;
	case 'Z':
		movimientoZ += aumento;
		break;
	}
}

int igvEscena3D::cajaCoincidente(GLubyte c[]) {
	for (int i = 0; i < vectorMalla.size(); i++) {
		if (abs(vectorMalla[i]->getColorByte()[0] - c[0]) <= 2 && abs(vectorMalla[i]->getColorByte()[1] - c[1]) <= 2 && abs(vectorMalla[i]->getColorByte()[2] - c[2]) <= 2) {
			return i;
		}
	}
	return -1;
}

void igvEscena3D::algoritmoGenerarNubes() {//vmalla23
	
}


void igvEscena3D::dibujaTexto(float x, float y, float z, char* texto) {
	glRasterPos3f(x, y, z);
	while (*texto) {
		glColor3d(1.0, 0.0, 0.0);
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *texto);
		texto++;
	}
}


void igvEscena3D::baseFlexo() {
	//GLfloat color_malla2[] = { 0.5,0.25,0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_flexo);
	glPushMatrix();
	glScaled(0.5, 0.2, 0.5);
	glutSolidCube(3);
	glPopMatrix();
	bolaBrazoFlexo();
}
void igvEscena3D::bolaBrazoFlexo() {
	//GLfloat color_malla2[] = { 0.5,0.25,0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_flexo);
	glPushMatrix();
	//glTranslated(0, 3, 0);
	glutSolidSphere(0.45, 50, 50);
	brazo1Flexo();
	glPopMatrix();
}
void igvEscena3D::brazo1Flexo() {
	//GLfloat color_malla2[] = { 0.5,0.25,0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_flexo);
	glPushMatrix();
	//glTranslated(0, 3, 0);
	glRotated(90, 1, 0, 0);
	glRotated(180, 0, 1, 0);
	glRotated(rotacionBrazoFlexo, 0, 1, 0);
	GLUquadric* cyl2 = gluNewQuadric();
	gluCylinder(cyl2, 0.5, 0.5, 3, 20, 1);
	bolaFlexo();
	glPopMatrix();
}

void igvEscena3D::bolaFlexo() {
	//GLfloat color_malla2[] = { 0.5,0.25,0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_flexo);
	glPushMatrix();
	glTranslated(0, 0, 3);
	glutSolidSphere(0.45, 50, 50);
	flexo();
	glPopMatrix();
}

void igvEscena3D::flexo() {
	//GLfloat color_malla2[] = { 0.5,0.25,0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, color_flexo);
	igvFuenteLuz foco(GL_LIGHT1, igvPunto3D(0.5, 0, 0.5), igvColor(0.0, 0.0, 0.0, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), igvColor(1.0, 1.0, 1.0, 1.0), 1.0, 0.0, 0.0, igvPunto3D(-1.0, -1.0, 0.0), 50, 0);
	glPushMatrix();
	glTranslated(0, 0, 0.3);
	glRotated(90, 0, 1, 0);
	glRotated(rotacionXFlexo, 1, 0, 0);
	glRotated(rotacionZFlexo, 0, 0, 1);
	if (fEncendido) {
		foco.aplicar();
	}
	else {
		glDisable(GL_LIGHT1);
	}
	glRotated(90, 0, 0, 1);
	glScaled(1.5, 0.1, 0.5);
	glutSolidCube(4);
	glPopMatrix();
}


////// Apartado C: a�adir aqu� los m�todos para modificar los grados de libertad del modelo
void igvEscena3D::aumentaZFlexo() {
	if (rotacionZFlexo < 210 && rotacionZFlexo >= 160) {
		GLdouble rota = 5;
		rotacionZFlexo += rota;
		std::cout << "AUMENTA Z FLEXO: " << rotacionZFlexo << std::endl;
		heRotadoZPositivo = true;
	}
}

void igvEscena3D::reduceZFlexo() {
	if (rotacionZFlexo <= 210 && rotacionZFlexo > 160) {
		GLdouble rota = -5;
		rotacionZFlexo += rota;
		std::cout << "REDUCE Z FLEXO: " << rotacionZFlexo << std::endl;
	}
}

void igvEscena3D::aumentaXFlexo() {
	GLdouble rota = 5;
	rotacionXFlexo += rota;
}

void igvEscena3D::disminuyeXFlexo() {
	GLdouble rota = 5;
	rotacionXFlexo -= rota;
}

void igvEscena3D::aumentaXFlexoAnimacion1() {
	//if (rotacionXFlexo < 25) {
	GLdouble rota = 0.05;
	rotacionXFlexo += rota;
	//}
}

void igvEscena3D::aumentaRotacionBrazoFlexo() {
	if (rotacionBrazoFlexo < 25) {
		GLdouble rota = 5;
		rotacionBrazoFlexo += rota;
	}
}

void igvEscena3D::reduceRotacionBrazoFlexo() {
	if (rotacionBrazoFlexo > -35) {
		GLdouble rota = -5;
		rotacionBrazoFlexo += rota;
	}
}

//GRAFO COCHE
void igvEscena3D::pinta_cilindro() {
	GLUquadricObj* tubo;
	GLfloat color_tubo[] = { 0,0,0.5 };

	glMaterialfv(GL_FRONT, GL_EMISSION, color_tubo);

	tubo = gluNewQuadric();
	gluQuadricDrawStyle(tubo, GLU_FILL);

	glPushMatrix();
	glTranslatef(0, 0, 0);
	gluCylinder(tubo, 0.5, 0.5, 0.2, 20, 20);
	glPopMatrix();

	gluDeleteQuadric(tubo);
}

void igvEscena3D::pinta_piramide() {
	vectorMalla[12]->visualizar();
}

void igvEscena3D::pinta_cuadrado() {
	glPushMatrix();
	glTranslated(0.5, 0, 0.5);
	glScaled(0.5, 1, 0.5);
	vectorMalla[13]->visualizar();
	glPopMatrix();
}

void igvEscena3D::pinta_tubo() {
	glPushMatrix();
	glTranslated(0.5, 0, 0.5);
	glScaled(0.5, 1, 0.5);
	vectorMalla[14]->visualizar();
	glPopMatrix();
}


void igvEscena3D::coche() {

	//base
	glPushMatrix();

	base();
	glPopMatrix();

}

void igvEscena3D::base() {
	glPushMatrix();
	glTranslated(0, 0.5, 0);
	ejedelantero();
	ejetrasero();
	mitadCoche();
	capoFrontal();
	capoTrasero();
	glScaled(6, 1, 2);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::mitadCoche() {
	//mitad del coche
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0, 0.625, 0);
	ventanaFrontal();
	ventanaTrasera();
	ventana1();
	ventana2();
	ventana3();
	ventana4();
	paloMitad();
	paloMitad2();
	palodelante();
	palodelante2();
	paloatras();
	paloatras2();

	glScaled(3, 0.25, 2);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::ventanaFrontal() {
	GLfloat negro[] = { 0,0,0 };
	glPushMatrix();
	glTranslated(-1, 0.125, 0);
	limpiaParabrisas();
	limpiaParabrisas2();
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glScaled(0.5, 0.6, 2);
	glRotated(180, 0, 1, 0);
	glTranslated(0, 0, -0.5);
	pinta_piramide();
	glPopMatrix();
}

void igvEscena3D::capoFrontal() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(-1.5, 0.5, 0);
	glRotated(180, 0, 1, 0);
	glScaled(1.5, 0.25, 2);
	glTranslated(0, 0, -0.5);
	pinta_piramide();
	glPopMatrix();
}

void igvEscena3D::ventanaTrasera() {
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(1, 0.125, 0);
	glScaled(0.5, 0.6, 2);
	glTranslated(0, 0, -0.5);
	pinta_piramide();
	glPopMatrix();
}

void igvEscena3D::capoTrasero() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(1.5, 0.5, 0);
	glScaled(1.5, 0.25, 2);
	glTranslated(0, 0, -0.5);
	pinta_piramide();
	glPopMatrix();
}

void igvEscena3D::paloMitad() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0, 0.3 + 0.125, 1 - 0.05);
	techo();
	glScaled(0.1, 0.6, 0.1);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::paloMitad2() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0, 0.3 + 0.125, -1 + 0.05);
	glScaled(0.1, 0.6, 0.1);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::palodelante() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(-1 + 0.05, 0.3 + 0.125, 1 - 0.05);
	glScaled(0.1, 0.6, 0.1);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}


void igvEscena3D::palodelante2() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(-1 + 0.05, 0.3 + 0.125, -1 + 0.05);
	glScaled(0.1, 0.6, 0.1);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::paloatras() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(1 - 0.05, 0.3 + 0.125, 1 - 0.05);
	glScaled(0.1, 0.6, 0.1);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}


void igvEscena3D::paloatras2() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(1 - 0.05, 0.3 + 0.125, -1 + 0.05);
	glScaled(0.1, 0.6, 0.1);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::techo() {
	GLfloat negro[] = { 1,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0, 0.3 + 0.075, -1 + 0.05);
	glScaled(2, 0.15, 2);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::ventana1() {
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(-1 + 0.1, 0.125, 1);
	glScaled(1 - 0.15, aperturaVentana, 0);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::ventana2() {
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0.05, 0.125, 1);
	glScaled(1 - 0.15, aperturaVentana, 0);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::ventana3() {
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(-1 + 0.1, 0.125, -1);
	glScaled(1 - 0.15, aperturaVentana, 0);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::ventana4() {
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0.05, 0.125, -1);
	glScaled(1 - 0.15, aperturaVentana, 0);
	pinta_cuadrado();
	glPopMatrix();
}

void igvEscena3D::limpiaParabrisas() {
	GLfloat blanco[] = { 1,1,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, blanco);
	glPushMatrix();
	glTranslated(-0.52, 0, 0.1);

	glRotated(-39, 0, 0, 1);//no se cambia
	glRotated(anguloParabrisas, 1, 0, 0);
	glScaled(0.05, sqrt(0.61), 0.05);
	glTranslated(-0.5, 0, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::limpiaParabrisas2() {
	GLfloat blanco[] = { 0.5,0.5,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, blanco);
	glPushMatrix();
	glTranslated(-0.52, 0, -0.8);

	glRotated(-39, 0, 0, 1);//no se cambia
	glRotated(anguloParabrisas, 1, 0, 0);
	glScaled(0.05, sqrt(0.61), 0.05);
	glTranslated(-0.5, 0, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::ejedelantero() {
	//parte interior
	glPushMatrix();
	glTranslated(-2, -0.5, 0);
	rueda1();
	rueda2();
	glRotated(rotaRuedaZ, 0, 0, 1);
	glScaled(0.125, 0.125, 2);
	glRotated(90, 1, 0, 0);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();

}


void igvEscena3D::ejetrasero() {
	//parte interior
	glPushMatrix();
	glTranslated(2, -0.5, 0);
	rueda1atras();
	rueda2atras();
	glRotated(rotaRuedaZ, 0, 0, 1);
	glScaled(0.125, 0.125, 2);
	glRotated(90, 1, 0, 0);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}


void igvEscena3D::rueda1() {
	//parte interior
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0, 0, 1.1);
	glRotated(rotaRuedaY, 0, 1, 0);
	rueda1int();
	glPopMatrix();
}

void igvEscena3D::rueda1int() {
	//parte esfera

	glPushMatrix();
	glRotated(rotaRuedaZ, 0, 0, 1);
	rueda1ext();
	rueda1int2();
	glScaled(0.25, 0.25, 0.25);
	glutSolidSphere(0.5, 30, 30);
	glPopMatrix();
}

void igvEscena3D::rueda1int2() {
	//parte interior
	glPushMatrix();
	generaRadiosLlantas();
	glPopMatrix();
}

void igvEscena3D::rueda1ext() {
	glPushMatrix();
	//pinta_cilindro();
	glScaled(1, 1, 0.2);
	glRotated(90, 1, 0, 0);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}


void igvEscena3D::rueda2() {
	//parte interior
	glPushMatrix();
	glTranslated(0, 0, -1.1);
	glRotated(rotaRuedaY, 0, 1, 0);
	rueda2int();
	glPopMatrix();
}

void igvEscena3D::rueda2int() {
	//parte esfera
	glPushMatrix();
	glRotated(rotaRuedaZ, 0, 0, 1);
	rueda2ext();
	rueda2int2();
	glScaled(0.25, 0.25, 0.25);
	glutSolidSphere(0.5, 30, 30);
	glPopMatrix();
}

void igvEscena3D::rueda2int2() {
	//parte interior
	glPushMatrix();
	generaRadiosLlantas();
	glPopMatrix();
}

void igvEscena3D::rueda2ext() {
	glPushMatrix();
	//pinta_cilindro();
	glScaled(1, 1, 0.2);
	glRotated(90, 1, 0, 0);
	glTranslated(-0.5, -0.5, -0.5);
	pinta_tubo();
	glPopMatrix();
}

void igvEscena3D::rueda1atras() {
	//parte interior
	GLfloat negro[] = { 0,0,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, negro);
	glPushMatrix();
	glTranslated(0, 0, 1.1);
	glRotated(-rotaRuedaY, 0, 1, 0);
	rueda1int();
	glPopMatrix();
}

void igvEscena3D::rueda2atras() {
	//parte interior
	glPushMatrix();
	glTranslated(0, 0, -1.1);
	glRotated(-rotaRuedaY, 0, 1, 0);
	rueda2int();
	glPopMatrix();
}


void igvEscena3D::generaRadiosLlantas() {
	glPushMatrix();
	float alpha = 360.0 / NUM_RADIOS;

	//Posiciones de los vertices
	int k = 0;


	for (int i = 0; i < NUM_RADIOS; i++) {//es <= ya que si tengo 2 niveles debo de ver el de arriba tambien
		float anguloRad = (i * alpha) * (M_PI / 180);
		glPushMatrix();
		glRotated(i * alpha, 0, 0, 1);
		glScaled(0.065, 0.5, 0.065);
		glTranslated(-0.5, 0, -0.5);
		pinta_tubo();
		glPopMatrix();
	}

	glPopMatrix();

}

void igvEscena3D::giraRueda(char sentido) {
	switch (sentido) {
	case '1':
		rotaRuedaZ += GRADO_ROTACION_RUEDAS_Z;
		break;
	case '2':
		rotaRuedaZ -= GRADO_ROTACION_RUEDAS_Z;
		break;
	case '3':
		rotaRuedaY += GRADO_ROTACION_RUEDAS;
		break;
	case '4':
		rotaRuedaY -= GRADO_ROTACION_RUEDAS;
		break;
	}
}





	

