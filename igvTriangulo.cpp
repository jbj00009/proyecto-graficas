#include "igvTriangulo.h"
#include <algorithm>

igvTriangulo::igvTriangulo() :igvMallaTriangulos()
{

}

igvTriangulo::igvTriangulo(float xMin, float zMin, float a, int divU, int divV, int tipo) : igvMallaTriangulos()
{
	this->tipo = tipo;
	num_vertices = 6;
	num_triangulos = 8;

	triangulos = new unsigned int[num_triangulos * 3];
	vertices = new float[num_vertices * 3];
	normales = new float[num_vertices * 3];

	smooth = false;
	verNormales = false;
	verNormales2 = false;

	vertices[0] = 0;
	vertices[1] = 0;
	vertices[2] = 0;

	vertices[3] = 0;
	vertices[4] = 0;
	vertices[5] = 1;

	vertices[6] = 1;
	vertices[7] = 0;
	vertices[8] = 0;

	vertices[9] = 1;
	vertices[10] = 0;
	vertices[11] = 1;

	vertices[12] = 0;
	vertices[13] = 1;
	vertices[14] = 0;

	vertices[15] = 0;
	vertices[16] = 1;
	vertices[17] = 1;

	for (int i = 0; i < num_vertices; i += 3) {
		normales[i] = vertices[i];
		normales[i + 1] = 0.00;
		normales[i + 2] = vertices[i + 2];
	}


	triangulos[0] = 3;
	triangulos[1] = 1;
	triangulos[2] = 0;

	triangulos[3] = 2;
	triangulos[4] = 3;
	triangulos[5] = 0;

	triangulos[6] = 4;
	triangulos[7] = 0;
	triangulos[8] = 5;
	
	triangulos[9] = 0;
	triangulos[10] = 1;
	triangulos[11] = 5;

	triangulos[12] = 4;
	triangulos[13] = 5;
	triangulos[14] = 3;

	triangulos[15] = 4;
	triangulos[16] = 2;
	triangulos[17] = 3;

	triangulos[18] = 0;
	triangulos[19] = 2;
	triangulos[20] = 4;

	triangulos[21] = 1;
	triangulos[22] = 3;
	triangulos[23] = 5;

	



}



igvTriangulo::~igvTriangulo()
{
}
