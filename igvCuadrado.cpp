#include "igvCuadrado.h"
#include <algorithm>
#include <iostream>

igvCuadrado::igvCuadrado() :igvMallaTriangulos()
{

}

igvCuadrado::igvCuadrado(float xMin, float zMin, float a, int divU, int divV, int tipo) : igvMallaTriangulos()
{
	this->tipo = tipo;

	num_vertices = 8;
	num_triangulos = 12;
	/*num_vertices = divU * (divV + 1);
	num_triangulos = 2 * divU * divV;*/

	triangulos = new unsigned int[num_triangulos * 3];
	vertices = new float[num_vertices * 3];
	normales = new float[num_vertices * 3];

	smooth = false;
	verNormales = false;
	verNormales2 = false;

	if (this->tipo != 3) {

		vertices[0] = -1;
		vertices[1] = 0;
		vertices[2] = -1;

		vertices[3] = -1;
		vertices[4] = 0;
		vertices[5] = 1;

		vertices[6] = 1;
		vertices[7] = 0;
		vertices[8] = 1;

		vertices[9] = 1;
		vertices[10] = 0;
		vertices[11] = -1;

		vertices[12] = -1;
		vertices[13] = a;
		vertices[14] = -1;

		vertices[15] = 1;
		vertices[16] = a;
		vertices[17] = -1;

		vertices[18] = -1;
		vertices[19] = a;
		vertices[20] = 1;

		vertices[21] = 1;
		vertices[22] = a;
		vertices[23] = 1;

		//double saltoX = abs(xMin) / divU;
		//double saltoZ = abs(zMin) / divU;
		//double xInicial = -xMin;
		//double zInicial = -zMin;

		//int k = 0;
		//for (int i = 0; i < divV + 1; i++) {//es <= ya que si tengo 2 niveles debo de ver el de arriba tambien
		//	for (int j = 0; j < divU; j++) {
		//		if (j < divU / 4) {
		//			//float anguloRad = (j * alpha) * (M_PI / 180);
		//			normales[k] = -xMin;
		//			vertices[k++] = -xMin;
		//			normales[k] = 0.00;
		//			vertices[k++] = (a / divV) * i;
		//			normales[k] = zInicial + (saltoZ*j);
		//			vertices[k++] = zInicial + (saltoZ * j);
		//		}
		//		else if (j < 2 * divU / 4) {
		//			normales[k] = xInicial + (saltoX * (j%(divU / 4)));
		//			vertices[k++] = xInicial + (saltoX * (j % (divU / 4)));
		//			normales[k] = 0.00;
		//			vertices[k++] = (a / divV) * i;
		//			normales[k] = zMin;
		//			vertices[k++] = zMin;
		//		}
		//		else if (j < 3 * divU / 4) {
		//			normales[k] = xMin;
		//			vertices[k++] = xMin;
		//			normales[k] = 0.00;
		//			vertices[k++] = (a / divV) * i;
		//			normales[k] = zInicial + (saltoZ * (j % (2*divU / 4)));
		//			vertices[k++] = zInicial + (saltoZ * (j % (2*divU / 4)));
		//		}
		//		else {
		//			normales[k] = xInicial + (saltoX * (j % (3*divU / 4)));
		//			vertices[k++] = xInicial + (saltoX * (j % (3*divU / 4)));
		//			normales[k] = 0.00;
		//			vertices[k++] = (a / divV) * i;
		//			normales[k] = -zMin;
		//			vertices[k++] = -zMin;
		//		}
		//	}
		//}

		for (int i = 0; i < num_vertices; i += 3) {
			normales[i] = vertices[i];
			normales[i + 1] = 0.00;
			normales[i + 2] = vertices[i + 2];
		}





		triangulos[0] = 1;
		triangulos[1] = 0;
		triangulos[2] = 2;

		triangulos[3] = 0;
		triangulos[4] = 3;
		triangulos[5] = 2;

		triangulos[6] = 0;
		triangulos[7] = 4;
		triangulos[8] = 3;

		triangulos[9] = 4;
		triangulos[10] = 5;
		triangulos[11] = 3;

		triangulos[12] = 6;
		triangulos[13] = 4;
		triangulos[14] = 1;

		triangulos[15] = 4;
		triangulos[16] = 0;
		triangulos[17] = 1;

		triangulos[18] = 2;
		triangulos[19] = 3;
		triangulos[20] = 7;

		triangulos[21] = 3;
		triangulos[22] = 5;
		triangulos[23] = 7;

		triangulos[24] = 6;
		triangulos[25] = 1;
		triangulos[26] = 7;

		triangulos[27] = 1;
		triangulos[28] = 2;
		triangulos[29] = 7;

		triangulos[30] = 4;
		triangulos[31] = 6;
		triangulos[32] = 5;

		triangulos[33] = 6;
		triangulos[34] = 7;
		triangulos[35] = 5;
	}else {

		vertices[0] = 0;
		vertices[1] = 0;
		vertices[2] = 0;

		vertices[3] = 0.5;
		vertices[4] = 0;
		vertices[5] = 0.5;

		vertices[6] = 1.5;
		vertices[7] = 0;
		vertices[8] = 0.5;

		vertices[9] = 1;
		vertices[10] = 0;
		vertices[11] = 0;

		vertices[12] = 0;
		vertices[13] = -0.2;
		vertices[14] = 0;

		vertices[15] = 0.5;
		vertices[16] = -0.2;
		vertices[17] = 0.5;

		vertices[18] = 1.5;
		vertices[19] = -0.2;
		vertices[20] = 0.5;

		vertices[21] = 1;
		vertices[22] = -0.2;
		vertices[23] = 0;


		for (int i = 0; i < num_vertices; i += 3) {
			normales[i] = vertices[i];
			normales[i + 1] = 0.00;
			normales[i + 2] = vertices[i + 2];
		}

		triangulos[0] = 3;
		triangulos[1] = 0;
		triangulos[2] = 1;

		triangulos[3] = 3;
		triangulos[4] = 1;
		triangulos[5] = 2;

		triangulos[6] = 1;
		triangulos[7] = 0;
		triangulos[8] = 5;

		triangulos[9] = 5;
		triangulos[10] = 0;
		triangulos[11] = 4;

		triangulos[12] = 2;
		triangulos[13] = 1;
		triangulos[14] = 5;

		triangulos[15] = 2;
		triangulos[16] = 5;
		triangulos[17] = 6;

		triangulos[18] = 3;
		triangulos[19] = 2;
		triangulos[20] = 7;

		triangulos[21] = 7;
		triangulos[22] = 2;
		triangulos[23] = 6;

		triangulos[24] = 0;
		triangulos[25] = 3;
		triangulos[26] = 4;

		triangulos[27] = 4;
		triangulos[28] = 3;
		triangulos[29] = 7;

		triangulos[30] = 5;
		triangulos[31] = 4;
		triangulos[32] = 6;

		triangulos[33] = 6;
		triangulos[34] = 4;
		triangulos[35] = 7;
	}

	////int cont = 0;
	////for (int i = 0; i < divV; i++) {
	////	for (int j = 0; j < divU; j++) {
	////		//triangulos abajo
	////		triangulos[cont++] = (divU * i) + ((j + 1) % divU);
	////		triangulos[cont++] = (divU * (i + 1)) + ((j + 1) % divU);
	////		triangulos[cont++] = (divU * i) + j;

	////		//triangulos arriba
	////		triangulos[cont++] = (divU * (i + 1)) + ((j + 1) % divU);
	////		triangulos[cont++] = (divU * (i + 1)) + (j % divU);
	////		triangulos[cont++] = (divU * i) + (j % divU);
	////	}
	////}

	//ALGORITMO NORMALES 2

	int vecesVerticeEnTriangulo = 0;//para hacer el promedio

	double Aprov[3];
	double Bprov[3];
	double A[3];
	double B[3];
	double prodVectorial[3];
	int ordenPrioridadVectorDirector[3];//ORDEN DE PRIORIDAD: si tenemos vertices (1,13,0) y quiero calcular normal respecto a v0, 0 primer lugar, los 2 lugares restantes por orden de menor a mayor



}



igvCuadrado::~igvCuadrado()
{
}
