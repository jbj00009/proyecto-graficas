#include "igvMallaTriangulos.h"
#include <math.h>
#define _USE_MATH_DEFINES
# define M_PI           3.14159265358979323846

class igvCuadrado : public igvMallaTriangulos {


public:

	//// Constructores por y destructor
	igvCuadrado();
	igvCuadrado(float xMin, float zMin, float a, int divU, int divV, int tipo);
	~igvCuadrado();

	//funciones propias

};

