#include "igvCilindro.h"
#include <algorithm>

igvCilindro::igvCilindro() :igvMallaTriangulos()
{
	
}

igvCilindro::igvCilindro(float r, float a, int divU, int divV, int tipo) : igvMallaTriangulos()
{
	this->tipo = tipo;
	/* Apartado B: Construir la malla de tri�ngulos para representar el cilindro */ //DIV U es num vertices por nivel, divV es num niveles
	float alpha = 360.0/divU;

	num_vertices=divU*(divV+1);
	num_triangulos = 2*divU*divV;

	triangulos = new unsigned int[num_triangulos * 3];
	vertices = new float[num_vertices * 3];
	normales = new float[num_vertices * 3];
	normales2 = new float[num_vertices * 3];

	//reseteo los valores
	for (int i = 0; i < num_vertices * 3; i++) {
		normales2[i] = 0;
	}

	smooth = false;
	verNormales = false;
	verNormales2 = false;

	//Posiciones de los vertices
	int k = 0;
	for (int i = 0; i < divV+1; i++) {//es <= ya que si tengo 2 niveles debo de ver el de arriba tambien
		for (int j = 0; j < divU; j++) {
			float anguloRad = (j*alpha) * (M_PI / 180);
			normales[k] = r*cosf(anguloRad);
			vertices[k++] = r * cos(anguloRad);
			normales[k] = 0.00;
			vertices[k++] = (a/divV) * i;
			normales[k] = r*sinf(anguloRad);
			vertices[k++] = r * sin(anguloRad);	
		}
	}

	
	//Posiciones de los triangulos
	//La figura tiene 2 niveles de altura, ergo tiene 4 triangulos dispuestos en vertical por cada unidad de ancho (2 triangulos por nivel de altura formando cuadrilatero)

	int cont = 0;
	for (int i = 0; i < divV; i++) {
		for (int j = 0; j < divU; j++) {
			//triangulos abajo
			triangulos[cont++] = (divU * i) + ((j + 1) % divU);
			triangulos[cont++] = (divU * (i + 1)) + ((j + 1) % divU);
			triangulos[cont++] = (divU * i) + j;

			//triangulos arriba
			triangulos[cont++] = (divU * (i + 1)) + ((j + 1) % divU);
			triangulos[cont++] = (divU*(i+1))+(j%divU);
			triangulos[cont++] = (divU*i)+(j%divU);
		}
	}

	for (int i = 0; i < 3; i++) {
		color[i] = colorQueSeVe[i] = colorQueSeVeRespaldo[i] = 0.0;
	}
	

	
	/* Apartado C: A�adir el vector de normales */
	//PARA VER LAS NORMALES EN SU PRIMER METODO PULSAR N, PARA VERLO EN EL METODO DEL PROMEDIO DE SUS NORMALES PRESIONAR M

	//El calculo de las normales se ha hecho al ellenar el vector de vertices. Este codigo de aqui abajo es un prototipo del calculo de la normal
	//mediante el promedio de todos los vectores directores que salen de los triangulos que contienen un determinado vertice

	//Basicamente para todo vertice comprobamos a que triangulos pertenece. Por ejemplo, triangulo 0 tiene los vertices 1,41,0. Necesito hacer dos vectores directores para asi calcular el producto vectorial y obtener la normal a ese triagulo que posteriormente normalizare
	//, es verdad que la normal se obtiene respecto al centro geometrico del triangulo pero al calcular el promedio de las normales de todos estos trangulos, sale la normal respecto al vertice
	//Este algoritmo auxiliar se activa presionando m y funciona bastante bien,
	//La complejidad del algoritmo radica en el calculo del producto vectorial. Las normales deben de salir del cilindro, por tanto en el producto vectorial es importantisimo el orden en el que se multipliquen
	//no es lo mismo AxB que BxA siendo A,B vectores normales, he solventado esto mediante un ingenioso algoritmo
	int vecesVerticeEnTriangulo = 0;//para hacer el promedio

	double Aprov[3];
	double Bprov[3];
	double A[3];
	double B[3];
	double prodVectorial[3];
	int ordenPrioridadVectorDirector[3];//ORDEN DE PRIORIDAD: si tenemos vertices (1,13,0) y quiero calcular normal respecto a v0, 0 primer lugar, los 2 lugares restantes por orden de menor a mayor

	for (int i = 0; i < num_vertices; i++) {
		vecesVerticeEnTriangulo = 0;
		double normal = 0;
		for (int j = 0; j < num_triangulos*3; j++) {
			if (triangulos[j] == i) {//si una coordenada del triangulo(vertice) es el vertice que busco lo a�ado. Normalizo el triangulo, necesito saber en cual estoy
				int trianguloEstoy = j / 3;

				ordenPrioridadVectorDirector[0]=triangulos[j];//el de referencia es el primero
				int c = 1;
				for (int z = 0; z < 3; z++) {
					if (triangulos[(3 * trianguloEstoy) + z] != i) {
						ordenPrioridadVectorDirector[c++] = triangulos[(3 * trianguloEstoy) + z];
					}
				}

				//compruebo el sentido oportuno del producto vectorial 

				
				Aprov[0] = vertices[3 * ordenPrioridadVectorDirector[1]] - vertices[3 * ordenPrioridadVectorDirector[0]];
				Aprov[1] = vertices[(3 * ordenPrioridadVectorDirector[1]) + 1] - vertices[(3 * ordenPrioridadVectorDirector[0]) + 1];
				Aprov[2] = vertices[(3 * ordenPrioridadVectorDirector[1]) + 2] - vertices[(3 * ordenPrioridadVectorDirector[0]) + 2];
				Bprov[0] = vertices[3 * ordenPrioridadVectorDirector[2]] - vertices[3 * ordenPrioridadVectorDirector[0]];
				Bprov[1] = vertices[(3 * ordenPrioridadVectorDirector[2]) + 1] - vertices[(3 * ordenPrioridadVectorDirector[0]) + 1];
				Bprov[2] = vertices[(3 * ordenPrioridadVectorDirector[2]) + 2] - vertices[(3 * ordenPrioridadVectorDirector[0]) + 2];

				prodVectorial[0] = (Aprov[1] * Bprov[2]) - (Aprov[2] * Bprov[1]);
				prodVectorial[1] = -((A[0] * B[2]) - (A[2] * B[0]));
				prodVectorial[2] = (Aprov[0] * Bprov[1]) - (Aprov[1] * Bprov[0]);//si z es positivo el orden de multiplicacion de los vectores directores esta bien, si no los invierto de lugar

				
				//Todo este bloque sirve para saber si el producto vectorial esta bien hecho, en funcion del signo de la actual normal de sus componentes

				if (vertices[(3 * ordenPrioridadVectorDirector[0]) + 2] > 0) {
					if (prodVectorial[2] < 0) {
						int aux = ordenPrioridadVectorDirector[1];
						ordenPrioridadVectorDirector[1] = ordenPrioridadVectorDirector[2];
						ordenPrioridadVectorDirector[2] = aux;
					}
				}
				else if (vertices[(3 * ordenPrioridadVectorDirector[0]) + 2] == 0) {
					if (vertices[3 * ordenPrioridadVectorDirector[0]] > 0) {
						if (prodVectorial[0] < 0) {
							int aux = ordenPrioridadVectorDirector[1];
							ordenPrioridadVectorDirector[1] = ordenPrioridadVectorDirector[2];
							ordenPrioridadVectorDirector[2] = aux;
						}
					}
					else {
						if (prodVectorial[0] > 0) {
							int aux = ordenPrioridadVectorDirector[1];
							ordenPrioridadVectorDirector[1] = ordenPrioridadVectorDirector[2];
							ordenPrioridadVectorDirector[2] = aux;
						}
					}
				}
				else {
					if (vertices[3 * ordenPrioridadVectorDirector[0]] == -r) {
						if (prodVectorial[0] > 0) {
							int aux = ordenPrioridadVectorDirector[1];
							ordenPrioridadVectorDirector[1] = ordenPrioridadVectorDirector[2];
							ordenPrioridadVectorDirector[2] = aux;
						}
					}
					else {
						if (prodVectorial[2] > 0) {//si es positivo esta mal ordenado la multiplicacion
							int aux = ordenPrioridadVectorDirector[1];
							ordenPrioridadVectorDirector[1] = ordenPrioridadVectorDirector[2];
							ordenPrioridadVectorDirector[2] = aux;
						}
					}
				}
				

				//vector director primero (ordenPrioridadVectorDirector(0)|ordenPrioridadVectorDirector(1)
				A[0] = vertices[ 3 * ordenPrioridadVectorDirector[1]] - vertices[3* ordenPrioridadVectorDirector[0]];
				A[1] = vertices[(3* ordenPrioridadVectorDirector[1]) + 1] - vertices[(3* ordenPrioridadVectorDirector[0]) + 1];
				A[2] = vertices[(3* ordenPrioridadVectorDirector[1]) + 2] - vertices[(3* ordenPrioridadVectorDirector[0]) + 2];
				//vector director segundo (ordenPrioridadVectorDirector(0)|ordenPrioridadVectorDirector(2)
				B[0] = vertices[3 * ordenPrioridadVectorDirector[2]] - vertices[3 * ordenPrioridadVectorDirector[0]];
				B[1] = vertices[(3 * ordenPrioridadVectorDirector[2]) + 1] - vertices[(3 * ordenPrioridadVectorDirector[0]) + 1];
				B[2] = vertices[(3 * ordenPrioridadVectorDirector[2]) + 2] - vertices[(3 * ordenPrioridadVectorDirector[0]) + 2];
				//producto vectorial de los 2 vectores
				prodVectorial[0] = (A[1] * B[2]) - (A[2] * B[1]);
				prodVectorial[1] = -((A[0] * B[2]) - (A[2] * B[0]));
				prodVectorial[2] = (A[0] * B[1]) - (A[1] * B[0]);
				//calculo la normal del producto vectorial
				normal= sqrt(pow(prodVectorial[0],2) + pow(prodVectorial[1], 2) + pow(prodVectorial[2], 2));
				//las normales las voy acumulando para luego hacer el promedio (divido entre el numero de normales por vertice)
				normales2[3 * i] += prodVectorial[0]/normal;
				normales2[(3 * i) + 1] += prodVectorial[1]/normal;
				normales2[(3 * i) + 2] += prodVectorial[2]/normal;


				vecesVerticeEnTriangulo++;
			}
		}

		normales2[3 * i] /= vecesVerticeEnTriangulo;
		normales2[(3 * i) + 1] /= vecesVerticeEnTriangulo;
		normales2[(3 * i) + 2] /= vecesVerticeEnTriangulo;
	}

	//imprimirInfo(r,a,divU,divV);
}


void igvCilindro::imprimirInfo(float r, float a, int divU, int divV) {
	int contRectangulos = 0;
	char nivActual = 'B';
	int* niveles = new int[divV];
	int nivelActual = 0;

	for (int i = 0; i < divV; i++) {
		niveles[i] = i;
	}

	for (int i = 0; i < num_triangulos * 3; i++) {
		if (i % (6*divV) == 0) {
			printf("\n---------RECTANGULO: %i-----------\n", contRectangulos);
			contRectangulos++;
		}
		printf("%i, ", triangulos[i]);

		if ((i + 1) % 6 == 0 && i > 0) {
			printf("     <--- Altura %i", niveles[nivelActual]);
			if (nivelActual + 1 == divV) {
				nivelActual = 0;
			}
			else {
				nivelActual++;
			}
		}

		if ((i + 1) % 3 == 0 && i > 0) {
			printf("\n");
			if ((i + 1) % 6 == 0) {
				printf("\n");
			}
		}
	}

	printf("**********************************************************\n");
	printf("VERTICES\n");
	for (int i = 1; i <= num_vertices*3; i++) {
		printf("%.3f,", vertices[i-1]);
		if (i % 3 == 0) {
			printf(" <--------- Vertice %i\n",(i/3)-1);
		}
	}
	printf("\n**********************************************************\n");
	printf("TRIANGULOS\n");
	for (int i = 0; i < num_triangulos*3; i++) {
		printf("%i,", triangulos[i]);
	}

	printf("\n**********************************************************\n");
	printf("NORMALES\n");
	for (int i = 1; i <= num_vertices * 3; i++) {
		printf("%.2f,", normales[i - 1]);
		if (i % 3 == 0) {
			printf(" <--------- Vertice %i\n", (i / 3) - 1);
		}
	}

	printf("\n\nNORMALES2\n");
	for (int i = 1; i <= num_vertices * 3; i++) {
		printf("%.2f,", normales2[i - 1]);
		if (i % 3 == 0) {
			printf(" <--------- Vertice %i\n", (i / 3) - 1);
		}
	}
	


	delete [] niveles;
}

igvCilindro::~igvCilindro()
{
}
