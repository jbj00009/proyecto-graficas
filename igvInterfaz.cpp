#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include "igvInterfaz.h"

extern igvInterfaz interfaz; // los callbacks deben ser estaticos y se requiere este objeto para acceder desde
                             // ellos a las variables de la clase

// Metodos constructores -----------------------------------

igvInterfaz::igvInterfaz () {}

igvInterfaz::~igvInterfaz () {}


// Metodos publicos ----------------------------------------

void igvInterfaz::crear_mundo(void) {
	// crear c�maras
	/*interfaz.camara.set(IGV_PARALELA, 
						igvPunto3D(origX,origY,origZ),
						igvPunto3D(refX,refY,refZ),
						igvPunto3D(vX,vY,vZ),
		                -1*1.5,
						1*1.5,
						-1*1.5,
						1*1.5,
						-1*3,
						200);*/
	interfaz.camara.set(IGV_PARALELA,
		igvPunto3D(camara.origX, camara.origY, camara.origZ),
		igvPunto3D(camara.refX, camara.refY, camara.refZ),
		igvPunto3D(camara.vX, camara.vY, camara.vZ),
		-1 * 5, 1 * 5, -1 * 5, 1 * 5, -5, 200);

	interfaz.camara.angulo = 60.0;
	interfaz.camara.raspecto = 1.0;

	vista = 0;
	vistaDividida = false;
	trasladaX = trasladaZ = 0;
}

void igvInterfaz::configura_entorno(int argc, char** argv,
			                              int _ancho_ventana, int _alto_ventana,
			                              int _pos_X, int _pos_Y,
													          string _titulo){
	// inicializaci�n de las variables de la interfaz																	
	ancho_ventana = _ancho_ventana;
	alto_ventana = _alto_ventana;

	// inicializaci�n de la ventana de visualizaci�n
	glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(_ancho_ventana,_alto_ventana);
  glutInitWindowPosition(_pos_X,_pos_Y);
	glutCreateWindow(_titulo.c_str());

	glEnable(GL_DEPTH_TEST); // activa el ocultamiento de superficies por z-buffer
  glClearColor(1.0,1.0,1.0,0.0); // establece el color de fondo de la ventana

	glEnable(GL_LIGHTING); // activa la iluminacion de la escena
  glEnable(GL_NORMALIZE); // normaliza los vectores normales para calculo iluminacion

	crear_mundo(); // crea el mundo a visualizar en la ventana
}

void igvInterfaz::inicia_bucle_visualizacion() {
	glutMainLoop(); // inicia el bucle de visualizacion de OpenGL
}

void igvInterfaz::set_glutKeyboardFunc(unsigned char key, int x, int y) {
	float anguloRad = interfaz.escena.getAnguloOrigen() * (M_PI / 180);
	if (interfaz.vistaDividida && !interfaz.camara.getModoCoche() && !interfaz.escena.get_animacion() && !interfaz.escena.get_animacion2()) { //Si se presiono antes el 4, en el modo vista dividida la ultima figura mostrada es la 3, ergo si quiero volver a resetear desde la vista 0, sumo una mas. 4 modulo 4= 0. Si no pongo esto hay un bug, al pulsar cualquier tecla avanzar�a 1 (como si presionara la v)
		interfaz.aumentarVista();
	}
	switch (key) {
	case 'o':
		interfaz.escena.rotarOreja1ConejoAbajo();
		interfaz.escena.rotarOreja2ConejoAbajo();
		break;
	case 'O':
		interfaz.escena.rotarOreja1ConejoArriba();
		interfaz.escena.rotarOreja2ConejoArriba();
		break;
	case 'c':
		interfaz.animacionConejo = !interfaz.animacionConejo;
		break;
	case 'C':
		interfaz.escena.rotarCabezaConejoArriba();
		break;
	case 'f':
		interfaz.escena.fEncendido = !interfaz.escena.fEncendido;
		break;
	case 'R'://Si el flexo est� seleccionado, puedes rotarlo, sino no
		if (interfaz.escena.flexoSeleccionado) {
			interfaz.escena.aumentaXFlexo();
		}
		break;
	case 'r'://Si el flexo est� seleccionado, puedes rotarlo, sino no
		if (interfaz.escena.flexoSeleccionado) {
			interfaz.escena.disminuyeXFlexo();
		}
		break;
    case 'x': // Apartado A: rotar X positivo 
		interfaz.escena.aumentaAngulo('X', interfaz.escena.get_rotacion());
		break;
    case 'X': // Apartado A: rotar X negativo 
		interfaz.escena.aumentaAngulo('X', -interfaz.escena.get_rotacion());
		break;
    case 'y': // Apartado A: rotar Y positivo 
		interfaz.escena.aumentaAngulo('Y', interfaz.escena.get_rotacion());
		break;
    case 'Y': // Apartado A: rotar y negativo 
		interfaz.escena.aumentaAngulo('Y', -interfaz.escena.get_rotacion());
		break;
    case 'z': // Apartado A: rotar z positivo 
		interfaz.escena.aumentaAngulo('Z', interfaz.escena.get_rotacion());
		break;
    case 'Z': // Apartado A: rotar Z negativo 
		interfaz.escena.aumentaAngulo('Z', -interfaz.escena.get_rotacion());
		break;
	case 'e': // activa/desactiva la visualizacion de los ejes
		interfaz.escena.set_ejes(interfaz.escena.get_ejes()?false:true);
		break;
	case 'g':
		if (!interfaz.escena.getVectorMalla()[0]->getSmooth()) {
			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setSmooth(true);
			}
		}
		else {
			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setSmooth(false);
			}
		}
		break;
	case 'G':
		if (!interfaz.escena.getVectorMalla()[0]->getSmooth()) {
			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setSmooth(true);
			}
		}
		else {
			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setSmooth(false);
			}
		}
		break;
	case 'n':
		if (!interfaz.escena.getVectorMalla()[0]->getverNormales()) {
			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setverNormales(true);
				interfaz.escena.getVectorMalla()[i]->setverNormales2(false);
			}
		}
		else {
			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setverNormales(false);
			}
		}
		break;
	case 'm':
		if (!interfaz.escena.get_animacion()) {
			interfaz.escena.set_animacion(true);
		}
		else {
			interfaz.escena.set_animacion(false);
		}
		break;
	case 'M':
		if (!interfaz.escena.get_animacion2()) {
			interfaz.escena.set_animacion2(true);
		}
		else {
			interfaz.escena.set_animacion2(false);
		}
		break;
	case 'p': // cambia el tipo de proyecci�n de paralela a perspectiva y viceversa
		if (interfaz.camara.tipo == IGV_PERSPECTIVA) {
			interfaz.camara.P0[0] /= interfaz.camara.factorMultPerspectiva;
			interfaz.camara.P0[1] /= interfaz.camara.factorMultPerspectiva;
			interfaz.camara.P0[2] /= interfaz.camara.factorMultPerspectiva;
			//interfaz.camara.aplicar();
			interfaz.camara.tipo = IGV_PARALELA;
		}
		else {
			interfaz.camara.P0[0] *= interfaz.camara.factorMultPerspectiva;
			interfaz.camara.P0[1] *= interfaz.camara.factorMultPerspectiva;
			interfaz.camara.P0[2] *= interfaz.camara.factorMultPerspectiva;
			//interfaz.camara.aplicar();
			interfaz.camara.tipo = IGV_PERSPECTIVA;
		}
		interfaz.camara.aplicar();
		break;
	case 'P': // cambia el tipo de proyecci�n de paralela a perspectiva y viceversa
		if (interfaz.camara.tipo == IGV_PERSPECTIVA) {
			interfaz.camara.tipo = IGV_PARALELA;
		}
		else {
			interfaz.camara.tipo = IGV_PERSPECTIVA;
		}
		interfaz.camara.aplicar();
		break;
	case 'v': // cambia la posici�n de la c�mara para mostrar las vistas planta, perfil, alzado o perspectiva
		if (!interfaz.vistaDividida) {
			interfaz.aumentarVista();
			if (interfaz.vista == 0) {
				interfaz.camara.P0 = igvPunto3D(3, 2, 4);
				interfaz.camara.V = igvPunto3D(0, 1, 0);
				interfaz.camara.aplicar();
			}
			else if (interfaz.vista == 1) {
				interfaz.camara.P0 = igvPunto3D(0, 8, 0); //donde pongo la camara
				interfaz.camara.V = igvPunto3D(-1, 0, 0); //Cuando miro desde arriba enfocando abajo el eje vertical pasa a ser el eje X
				interfaz.camara.aplicar();
			}
			else if (interfaz.vista == 2) {
				interfaz.camara.P0 = igvPunto3D(3, 0, 0); //donde pongo la camara
				interfaz.camara.V = igvPunto3D(0, 1, 0);
				interfaz.camara.aplicar();
			}
			else {
				interfaz.camara.P0 = igvPunto3D(0, 0, 4); //donde pongo la camara
				interfaz.camara.V = igvPunto3D(0, 1, 0);
				interfaz.camara.aplicar();
			}
			printf("<<Vista actual: %i>> \n", interfaz.vista);
		}
		else {
			interfaz.aumentarVista();
		}
		break;
	case 'V': // cambia la posici�n de la c�mara para mostrar las vistas planta, perfil, alzado o perspectiva
		interfaz.disminuirVista();
		if (interfaz.vista == 0) {
			interfaz.camara.P0 = igvPunto3D(3, 2, 4);
			interfaz.camara.aplicar();
		}
		else if (interfaz.vista == 1) {
			interfaz.camara.P0 = igvPunto3D(0.001, 6, 0); //donde pongo la camara
			interfaz.camara.aplicar();
		}
		else if (interfaz.vista == 2) {
			interfaz.camara.P0 = igvPunto3D(3, 0, 0); //donde pongo la camara
			interfaz.camara.aplicar();
		}
		else {
			interfaz.camara.P0 = igvPunto3D(0, 0, 4); //donde pongo la camara
			interfaz.camara.aplicar();
		}
		//printf("<<Vista actual: %i>> \n", interfaz.vista);
		break;
	case '+': // zoom in
		interfaz.camara.zoom(1 / interfaz.camara.FACTOR_ZOOM);
		interfaz.camara.zoomAcumulado += (interfaz.camara.FACTOR_ZOOM - 1) * 100;
		//printf("Zoom: %.0f %% \n", interfaz.camara.zoomAcumulado);
		break;
	case '-': // zoom out
		interfaz.camara.zoom(interfaz.camara.FACTOR_ZOOM);
		interfaz.camara.zoomAcumulado -= (interfaz.camara.FACTOR_ZOOM - 1) * 100;
		//printf("Zoom: %.1f %% \n", interfaz.camara.zoomAcumulado);
		break;
	case '1': // incrementar la distancia del plano cercano
		interfaz.camara.cambioznear += 0.2;
		interfaz.camara.aplicar();
		//printf("Dist Plano cercano(Znear): %.2f \n", interfaz.camara.znear);
		break;
	case '2': // decrementar la distancia del plano cercano
		interfaz.camara.cambioznear -= 0.2;
		interfaz.camara.aplicar();
		//printf("Dist Plano cercano(Znear): %.2f \n", interfaz.camara.znear);
		break;
	case '4': // dividir la ventana  en cuatro vistas
		if (!interfaz.vistaDividida) {
			interfaz.vistaDividida = true;
		}
		else {
			interfaz.vistaDividida = false;
			interfaz.vista = 0; //cuando vuelva de la vision de 4 figuras a la visualizacion unica, vuelve a mostrar la primera figura (no puedo reutilizar el primer aumentarVista de la funcion ya que solo funcionaria si la figura de abajo a la derecha fuera la 3, si es la 0 o 1 o 2 no funcionar�a)
			interfaz.algdividirVista(); //reinicio a la vista 0
		}
		interfaz.camara.movimientoAcumulado = 0;
		break;
	case 'a':
		interfaz.camara.movimientoCamara(1); //1 es en horario respeco el eje y
		interfaz.camara.aplicar();
		break;
	case 'd':
		interfaz.camara.movimientoCamara(2); //2 es en antihorario respecto eje y
		interfaz.camara.aplicar();
		break;
	case 'w':
		interfaz.camara.movimientoCamara(3); //3 es en horario respecto eje x
		interfaz.camara.aplicar();
		break;
	case 's':
		interfaz.camara.movimientoCamara(4); //4 es en antihorario respecto eje x
		interfaz.camara.aplicar();
		break;
	case 'A':
		interfaz.escena.desplazaObjeto('X',-interfaz.escena.get_movimiento());
		break;
	case 'D':
		interfaz.escena.desplazaObjeto('X', interfaz.escena.get_movimiento());
		break;
	case 'W':
		interfaz.escena.desplazaObjeto('Y', interfaz.escena.get_movimiento());
		break;
	case 'S':
		interfaz.escena.desplazaObjeto('Y', -interfaz.escena.get_movimiento());
		break;
	case 'q':
		interfaz.escena.desplazaObjeto('Z', interfaz.escena.get_movimiento());
		break;
	case 'Q':
		interfaz.escena.desplazaObjeto('Z', -interfaz.escena.get_movimiento());
		break;
	case 'i':
		interfaz.escena.giraRueda('1');
		if (interfaz.escena.getFactorGiro() != 0) {
			interfaz.escena.setAnguloOrigen(interfaz.escena.getAnguloOrigen() + (interfaz.escena.get_rotacion_ruedas() * interfaz.escena.getFactorGiro()));
		}
		interfaz.escena.setPosXCoche(interfaz.escena.getPosXCoche() + (interfaz.escena.getVelocidad() * cosf(anguloRad)));
		interfaz.escena.setPosYCoche(interfaz.escena.getPosYCoche() + (interfaz.escena.getVelocidad() * sinf(anguloRad)));

		interfaz.camara.setCoche(igvPunto3D(interfaz.escena.getPosXCoche(),1.82, interfaz.escena.getPosYCoche()));
		break;
	case 'k':
		interfaz.escena.giraRueda('2');
		if (interfaz.escena.getFactorGiro() != 0) {
			interfaz.escena.setAnguloOrigen(interfaz.escena.getAnguloOrigen() - (interfaz.escena.get_rotacion_ruedas() * interfaz.escena.getFactorGiro()));
		}
		interfaz.escena.setPosXCoche(interfaz.escena.getPosXCoche() - (interfaz.escena.getVelocidad() * cosf(anguloRad)));
		interfaz.escena.setPosYCoche(interfaz.escena.getPosYCoche() - (interfaz.escena.getVelocidad() * sinf(anguloRad)));

		interfaz.camara.setCoche(igvPunto3D(interfaz.escena.getPosXCoche(), 1.82, interfaz.escena.getPosYCoche()));
		break;
	case 'j':
		if (interfaz.escena.get_rotaY() < (360 / 12)) {
			interfaz.escena.giraRueda('3');
			interfaz.escena.setFactorGiro(interfaz.escena.getFactorGiro() - 0.25);
		}
		break;
	case 'l':
		if (interfaz.escena.get_rotaY() > -(360 / 12)) {
			interfaz.escena.giraRueda('4');
			interfaz.escena.setFactorGiro(interfaz.escena.getFactorGiro() + 0.25);
		}
		break;
	case '6':
		interfaz.camara.setModoCoche(!interfaz.camara.getModoCoche());
		if (interfaz.camara.getModoCoche()) {
			interfaz.camara.setCoche(igvPunto3D(interfaz.escena.getPosXCoche(), 1.82, interfaz.escena.getPosYCoche()));
		}
		break;
	case '7':
		interfaz.escena.setTexto(!interfaz.escena.getTexto());
		break;
	case '8':
		if (interfaz.objeto_seleccionado != -1) {
			interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->aumentaRotacion(9);
		}
		break;
	case '9':
		if (interfaz.objeto_seleccionado != -1) {
			interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->aumentaRotacion(-9);
		}
		break;

    case 27: // tecla de escape para SALIR
		exit(1);
	break;
  }
	glutPostRedisplay(); // renueva el contenido de la ventana de vision
}

void igvInterfaz::set_glutReshapeFunc(int w, int h) {
  // dimensiona el viewport al nuevo ancho y alto de la ventana
  // guardamos valores nuevos de la ventana de visualizacion
  interfaz.set_ancho_ventana(w);
  interfaz.set_alto_ventana(h);

	// establece los par�metros de la c�mara y de la proyecci�n
	interfaz.camara.aplicar();
}

void igvInterfaz::set_glutDisplayFunc() {

	//display

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // borra la ventana y el z-buffer
		if (!interfaz.vistaDividida) {
			// se establece el viewport
			glViewport(0, 0, interfaz.get_ancho_ventana(), interfaz.get_alto_ventana());
		}
		else {
			interfaz.dividirVista();
		}

		if (interfaz.modo == IGV_SELECCIONAR) {
			glDisable(GL_LIGHTING); // desactiva la iluminacion de la escena
			glDisable(GL_DITHER);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_CULL_FACE);

			// Reestablece los colores como no seleccionado

			for (int i = 0; i < interfaz.escena.getVectorMalla().size(); i++) {
				interfaz.escena.getVectorMalla()[i]->setSeleccionar(false);
			}

			// aplica la c�mara
			interfaz.camara.aplicar();

			// visualiza los BV cada uno de un color
			interfaz.escena.visualizarVB();
			// Obtener el color del pixel seleccionado
			GLubyte pixel[3];
			glReadPixels(interfaz.cursorX, interfaz.alto_ventana - interfaz.cursorY, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &pixel);

			// Comprobar el color del objeto que hay en el cursor mirando en la tabla de colores y asigna otro color al objeto seleccionado
			interfaz.objeto_seleccionado = interfaz.escena.cajaCoincidente(pixel);

			if (interfaz.objeto_seleccionado != -1) {
				if (interfaz.objeto_seleccionado == 9) {
					interfaz.escena.flexoSeleccionado = true;
				}
				else {
					interfaz.escena.flexoSeleccionado = false;
				}
				interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->setSeleccionar(true);
			}

			// Cambiar a modo de visualizaci�n de la escena
			interfaz.modo = IGV_VISUALIZAR;

			//  Habilitar de nuevo la iluminaci�n
			glEnable(GL_LIGHTING);
		}
		else {
			// aplica las transformaciones en funci�n de los par�metros de la c�mara
			interfaz.camara.aplicar();
			// visualiza la escena
			interfaz.escena.visualizar();

			// refresca la ventana
			glutSwapBuffers();
		}
		////visualiza la escena
		//interfaz.escena.visualizar();

		//// refresca la ventana
		//glutSwapBuffers(); // se utiliza, en vez de glFlush(), para evitar el parpadeo
		if (!interfaz.vistaDividida) {
			glutPostRedisplay();
		}

		interfaz.frame_count++;
		interfaz.final_time = time(NULL);
		if (interfaz.final_time - interfaz.initial_time >= 1) {//ha pasado un segundo, calculamos framerate
			interfaz.FPS = interfaz.frame_count / (interfaz.final_time - interfaz.initial_time);
			interfaz.escena.setFPS(interfaz.FPS);
			if (interfaz.FPS > 1) {
				std::cout << "Frames por Segundo (FPS): " << interfaz.FPS << std::endl;
			}
			interfaz.frame_count = 0;
			interfaz.initial_time = interfaz.final_time;
		}

}


void igvInterfaz::set_glutMouseFunc(GLint boton, GLint estado, GLint x, GLint y) {

	// Apartado A: comprobar que se ha pulsado el bot�n izquierdo 
	if (boton == GLUT_LEFT_BUTTON) {
		if (estado == GLUT_DOWN) {
			interfaz.modo = IGV_SELECCIONAR;
			interfaz.cursorX = x;
			interfaz.cursorY = y;
			interfaz.boton_retenido = true;
			//printf("%i, %i \n",interfaz.cursorX,interfaz.cursorY);
		}
		else {
			if (interfaz.objeto_seleccionado != -1) {
				//printf("es: %f \n", interfaz.trasladaZ / 100);
				interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->setorigX(interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->getdifX());
				interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->setorigZ(interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->getdifZ());
				interfaz.boton_retenido = false;
			}
		}
	}
	// Apartado A: guardar que el boton se ha presionado o se ha soltado, si se ha pulsado hay que
	// pasar a modo IGV_SELECCIONAR

	// Apartado A: guardar el pixel pulsado

	// Apartado A: renovar el contenido de la ventana de vision
	// 
	glutPostRedisplay();
	//set_glutDisplayFunc();
}
void igvInterfaz::set_glutMotionFunc(GLint x, GLint y) {

	// Apartado B: si el bot�n est� retenido y hay alg�n objeto seleccionado,
	// comprobar el objeto seleccionado y la posici�n del rat�n y rotar
	// el objeto seleccionado utilziando el desplazamiento entre la posici�n 
	//inicial y final del rat�n
	if (interfaz.boton_retenido) {
		if (interfaz.objeto_seleccionado != -1) {
			interfaz.trasladaZ = -(x - interfaz.cursorX);
			interfaz.trasladaX = y - interfaz.cursorY;
			interfaz.escena.mueveObjeto(interfaz.objeto_seleccionado, interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->getorigX() + (interfaz.trasladaX / 100), interfaz.escena.getVectorMalla()[interfaz.objeto_seleccionado]->getorigZ() + interfaz.trasladaZ / 100);
		}
	}

	// Apartado B: guardar la nueva posici�n del rat�n
	// Apartado B: renovar el contenido de la ventana de vision 
}

void igvInterfaz::algdividirVista() {
	if (interfaz.vista == 0) {
		interfaz.camara.P0 = igvPunto3D(3, 2, 4);
		interfaz.camara.V = igvPunto3D(0, 1, 0);
		interfaz.camara.aplicar();
	}
	else if (interfaz.vista == 1) {
		interfaz.camara.P0 = igvPunto3D(0, 6, 0); //donde pongo la camara
		interfaz.camara.V = igvPunto3D(1, 0, 0);
		interfaz.camara.aplicar();
	}
	else if (interfaz.vista == 2) {
		interfaz.camara.P0 = igvPunto3D(3, 0, 0); //donde pongo la camara
		interfaz.camara.V = igvPunto3D(0, 1, 0);
		interfaz.camara.aplicar();
	}
	else {
		interfaz.camara.P0 = igvPunto3D(0, 0, 4); //donde pongo la camara
		interfaz.camara.V = igvPunto3D(0, 1, 0);
		interfaz.camara.aplicar();
	}
	//printf("<<Vista actual: %i>> \n", interfaz.vista);
}

void igvInterfaz::dividirVista() {
	glViewport(0, interfaz.get_ancho_ventana() / 2, interfaz.get_ancho_ventana() / 2, interfaz.get_alto_ventana() / 2);
	algdividirVista();
	interfaz.escena.visualizar();

	glViewport(interfaz.get_ancho_ventana() / 2, interfaz.get_alto_ventana() / 2, interfaz.get_ancho_ventana() / 2, interfaz.get_alto_ventana() / 2);
	aumentarVista();
	algdividirVista();
	interfaz.escena.visualizar();

	glViewport(0, 0, interfaz.get_ancho_ventana() / 2, interfaz.get_alto_ventana() / 2);
	aumentarVista();
	algdividirVista();
	interfaz.escena.visualizar();

	glViewport(interfaz.get_ancho_ventana() / 2, 0, interfaz.get_ancho_ventana() / 2, interfaz.get_alto_ventana() / 2);
	aumentarVista();
	algdividirVista();
}

void igvInterfaz::aumentarVista() {
	if (vista == 3) {
		vista = 0;
	}
	else {
		vista++;
	}
}

void igvInterfaz::disminuirVista() {
	if (vista == 0) {
		vista = 3;
	}
	else {
		vista--;
	}
}

void igvInterfaz::set_glutIdleFunc() {
	//ajusto la velocidad a cada PC
	double VELOCIDAD=1;
	double VELOCIDAD2 = 1;
	if (interfaz.FPS > 0) {
		VELOCIDAD = 600 / interfaz.FPS;
		VELOCIDAD2 = 200 / interfaz.FPS;
	}


	///// Apartado D: incluir el c�digo para animar el modelo de la manera m�s realista posible
	if (interfaz.escena.get_animacion()) {

		if (interfaz.escena.get_sentidoLimpiaParabrisas() == 0) {
			//printf("angulo %.1f: \n",interfaz.escena.get_anguloLimpiaparabrisas());
			interfaz.escena.set_anguloLimpiaparabrisas(interfaz.escena.get_anguloLimpiaparabrisas() - (0.1 * VELOCIDAD));
			if (interfaz.escena.get_anguloLimpiaparabrisas() < -20) {
				interfaz.escena.set_sentidoLimpiaParabrisas(1);
			}
		}
		else {

			interfaz.escena.set_anguloLimpiaparabrisas(interfaz.escena.get_anguloLimpiaparabrisas() + (0.1 * VELOCIDAD));
			if (interfaz.escena.get_anguloLimpiaparabrisas() > 90) {
				interfaz.escena.set_sentidoLimpiaParabrisas(0);
			}
		}

		if (interfaz.escena.get_sentidoVentana() == 0) {
			interfaz.escena.set_aperturaVentana(interfaz.escena.get_aperturaVentana() - (0.0001*VELOCIDAD));
			if (interfaz.escena.get_aperturaVentana() < 0) {
				interfaz.escena.set_sentidoVentana(1);
			}
		}
		else {
			interfaz.escena.set_aperturaVentana(interfaz.escena.get_aperturaVentana() + (0.0001 * VELOCIDAD));
			if (interfaz.escena.get_aperturaVentana() > 0.6) {
				interfaz.escena.set_sentidoVentana(0);
			}
		}
		glutPostRedisplay();
	}

	if (interfaz.escena.get_animacion2()) {
		if (!interfaz.escena.get_giraCabeza()) {
			interfaz.escena.setanguloCabeza(interfaz.escena.getanguloCabeza()+(0.2*VELOCIDAD2));
			if (interfaz.escena.getanguloCabeza() > 40) {
				interfaz.escena.set_giraCabeza(true);
			}
		}
		else {
			interfaz.escena.setanguloCabeza(interfaz.escena.getanguloCabeza() - (0.2 * VELOCIDAD2));
			if (interfaz.escena.getanguloCabeza() < -40) {
				interfaz.escena.set_giraCabeza(false);
			}
		}

		if (!interfaz.escena.get_giraOrejaAzul()) {
			interfaz.escena.setanguloOrejaAzul(interfaz.escena.getanguloOrejaAzul() + (0.1 * VELOCIDAD2));
			if (interfaz.escena.getanguloOrejaAzul() > 5) {
				interfaz.escena.set_giraOrejaAzul(true);
			}
		}
		else {
			interfaz.escena.setanguloOrejaAzul(interfaz.escena.getanguloOrejaAzul() - (0.1 * VELOCIDAD2));
			if (interfaz.escena.getanguloOrejaAzul() < -15) {
				interfaz.escena.set_giraOrejaAzul(false);
			}
		}

		if (!interfaz.escena.get_giraBase()) {
			interfaz.escena.setanguloBase(interfaz.escena.getanguloBase() - (0.02 * VELOCIDAD2));
			if (interfaz.escena.getanguloBase() < -10) {
				interfaz.escena.set_giraBase(true);
			}
		}
		else {
			interfaz.escena.setanguloBase(interfaz.escena.getanguloBase() + (0.02 * VELOCIDAD2));
			if (interfaz.escena.getanguloBase() > 10) {
				interfaz.escena.set_giraBase(false);
			}
		}

		if (!interfaz.escena.get_giraCabeza2()) {
			interfaz.escena.setanguloCabeza2(interfaz.escena.getanguloCabeza2() + (0.07 * VELOCIDAD2));
			if (interfaz.escena.getanguloCabeza2() > 20) {
				interfaz.escena.set_giraCabeza2(true);
			}
		}
		else {
			interfaz.escena.setanguloCabeza2(interfaz.escena.getanguloCabeza2() - (0.07 * VELOCIDAD2));
			if (interfaz.escena.getanguloCabeza2() < 0) {
				interfaz.escena.set_giraCabeza2(false);
			}
		}
		glutPostRedisplay();
	}

	if (interfaz.animacionConejo) {
		if (!interfaz.escena.cambioAnimacion) {
			interfaz.escena.rotarOreja1ConejoAbajo();
			interfaz.escena.rotarOreja2ConejoAbajo();
			if (!interfaz.escena.cambioCabezaConejo) {
				interfaz.escena.rotarCabezaConejoAbajo();
			}
			else {
				interfaz.escena.rotarCabezaConejoArriba();
			}
		}
		else {
			interfaz.escena.rotarOreja1ConejoArriba();
			interfaz.escena.rotarOreja2ConejoArriba();
			if (!interfaz.escena.cambioCabezaConejo) {
				interfaz.escena.rotarCabezaConejoAbajo();
			}
			else {
				interfaz.escena.rotarCabezaConejoArriba();
			}
		}
	}

}


void igvInterfaz::inicializa_callbacks() {
	glutKeyboardFunc(set_glutKeyboardFunc);
	glutReshapeFunc(set_glutReshapeFunc);
	glutDisplayFunc(set_glutDisplayFunc);
	glutMouseFunc(set_glutMouseFunc);
	glutMotionFunc(set_glutMotionFunc);
	glutIdleFunc(set_glutIdleFunc);
}