#ifndef __IGVCAMARA
#define __IGVCAMARA

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#include "igvPunto3D.h"
#include <stdio.h>
# define M_PI           3.14159265358979323846

typedef enum {
	IGV_PARALELA,
	IGV_FRUSTUM,
	IGV_PERSPECTIVA
} tipoCamara;

class igvCamara {

	public:
		// atributos

		tipoCamara tipo;	// paralela o perspectiva

		// ventana de visi�n: parametros proyecci�n paralela y frustum
		GLdouble xwmin, xwmax, ywmin, ywmax;

		// ventana de visi�n: parametros proyecci�n perspectiva
		GLdouble 	angulo, raspecto;

		// distancias de planos cercano y lejano
		GLdouble znear, zfar, cambioznear;

		// punto de visi�n
		igvPunto3D P0;

		// punto de referencia de visi�n
		igvPunto3D r;

		// vector arriba
		igvPunto3D V;

		//CONSTANTES
		//P0
		const double origX = 4.0;
		const double origY = 2.0;
		const double origZ = 4.0;
		//P referencia
		const double refX = 0;
		const double refY = 0;
		const double refZ = 0;
		//Vertical
		const double vX = 0;
		const double vY = 1;
		const double vZ = 0;

		const double FACTOR_ZOOM = 1.05; //constante parametrizable 5% de zoom
		const double RADIO = origX; //radio en el cual coloco la camara
		const int SALTO_MOVIMIENTO_CAMARA = 100; //cuanto mas grande sea esto, mas peque�o sera el salto al darle al awsd
		const int factorMultPerspectiva = 2;

		//ATRIBUTOS DEL ALGORITMO
		int movimientoAcumulado = 0;
		int movimientoAcumulado2 = 0;
		double zoomAcumulado; //para saber el zoom que llevo y mostrarlo por pantalla
		bool subirY;
		bool bajarY;

		
		//CAMARA COCHE
		igvPunto3D Pcoche;
		bool modoCoche;
		// Metodos

	public:
		// Constructores por defecto y destructor
		igvCamara();
		~igvCamara();

		// Otros constructores
		igvCamara(tipoCamara _tipo, igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V);

		// Metodos
		// define la posici�n de la c�mara
		void set(igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V);

		// define una c�mara de tipo paralela o frustum
		void set(tipoCamara _tipo, igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V,
			                         double _xwmin, double _xwmax, double _ywmin, double _ywmax, double _znear, double _zfar);

		// define una c�mara de tipo perspectiva
		void set(tipoCamara _tipo, igvPunto3D _P0, igvPunto3D _r, igvPunto3D _V,
			                         double _angulo, double _raspecto, double _znear, double _zfar);

		void aplicar(void); // aplica a los objetos de la escena la transformaci�n de visi�n y la transformaci�n de proyecci�n
		                    // asociadas a los par�metros de la c�mara
		void zoom(double factor); // realiza un zoom sobre la c�mara

		void movimientoCamara(int sentido);

		void setModoCoche(bool valor) {
			modoCoche = valor;
		}

		bool getModoCoche() { return modoCoche; };

		void setCoche(igvPunto3D _Pcoche);
};

#endif

