#ifndef __IGVMALLATRIANGULOS
#define __IGVMALLATRIANGULOS

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#include <string>

class igvMallaTriangulos {
	protected:
		// Atributos

		long int num_vertices; // n�mero de v�rtices de la malla de tri�ngulos
		float *vertices; // array con las (num_vertices * 3) coordenadas de los v�rtices
		float *normales; // array con las (num_vertices * 3) coordenadas de la normal a cada v�rtice (s�lo para la generaci�n de la esfera)
		float *normales2; // array con las (num_vertices * 3) coordenadas de la normal a cada v�rtice (s�lo para la generaci�n de la esfera) aqui se usa un metodo propio del promedio de todas las normales de un vertice

		long int num_triangulos; // n�mero de tri�ngulos de la malla de tri�ngulos
		unsigned int *triangulos; // array con los (num_triangulos * 3) �ndices a los v�rtices de cada tri�ngulo

		//atributos propios
		bool smooth;
		bool verNormales;
		bool verNormales2;

		//atributos de seleccion
		float* color=new float[3];//es el color con el que trabaja el buffer de color
		float* colorQueSeVe=new float[3];
		float* colorQueSeVeRespaldo = new float[3];

		bool seleccionada;

		//int direccion;
		//int gradosRotacion;

		double difX;
		double difZ;

		double origX;
		double origZ;

		int tipo;//0 es malla,1 no es malla

		//VARIABLES ROTACION
		int rotacion;


	public:
		// Constructor y destructor
		
		igvMallaTriangulos();

		igvMallaTriangulos(long int _num_vertices, float *_vertices, long int _num_triangulos, unsigned int *_triangulos,int tipo);

		~igvMallaTriangulos();

		// M�todo con las llamadas OpenGL para visualizar la malla de tri�ngulos
		void visualizar();

		//metodo para saber si quiero smooth
		bool getSmooth() { return smooth; };
		void setSmooth(bool valor) { this->smooth=valor; };

		//metodo para saber si quiero ver normales
		bool getverNormales() { return verNormales; };
		void setverNormales(bool valor) { this->verNormales = valor; };

		bool getverNormales2() { return verNormales2; };
		void setverNormales2(bool valor) { this->verNormales2 = valor; };

		//metodos de seleccion
		void setColorAlgoritmo(float r, float g, float b);
		void setColor(float r, float g, float b);
		void setColorRespaldo(float r, float g, float b);

		float* getColorAlgoritmo() { return color; };
		float* getColorRespaldo() { return colorQueSeVeRespaldo; };
		float* getColorQueSeVe() { return colorQueSeVe; };
		GLubyte* getColorByte(); //Para comparar con el pixel obtenido en la selecci�n
		void setSeleccionar(bool estado);

		double getdifX() { return difX; };
		double getdifZ() { return difZ; };

		void setdifX(double valor) { difX = valor; };
		void setdifZ(double valor) { difZ = valor; };

		double getorigX() { return origX; };
		double getorigZ() { return origZ; };

		void setorigX(double valor) { origX = valor; };
		void setorigZ(double valor) { origZ = valor; };

		int getRotacion() { return rotacion; };
		void aumentaRotacion(int valor) {
			if (rotacion + valor > 360) {
				rotacion = (rotacion + valor) % 360;
			}
			else {
				rotacion += valor;
			}
			;
		};




};

#endif
