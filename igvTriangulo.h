#include "igvMallaTriangulos.h"
#include <math.h>
#define _USE_MATH_DEFINES
# define M_PI           3.14159265358979323846

class igvTriangulo : public igvMallaTriangulos {


public:

	//// Constructores por y destructor
	igvTriangulo();
	igvTriangulo(float xMin, float zMin, float a, int divU, int divV, int tipo);
	~igvTriangulo();

	//funciones propias

};

