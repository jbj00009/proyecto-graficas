#ifndef __IGVESCENA3D
#define __IGVESCENA3D

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#pragma warning(disable:4996)

#include "igvMallaTriangulos.h"
#include "igvCilindro.h"
#include "igvCuadrado.h"
#include "igvTriangulo.h"
#include "Pieza.h"
#include "igvTextura.h"

#include <string>
#include <vector>

class igvEscena3D {
protected:
	// Atributos
	bool ejes;

	//Atributos propios
	const double GRADO_MOVIMIENTO = 0.2;//grado de movimiento al presionar W,A,S,D
	double movimientoX;
	double movimientoY;
	double movimientoZ;
	

	GLdouble movX = 0;

	std::vector<igvMallaTriangulos*> vectorMalla;

	GLfloat* vColores;
	int incrementoParticiones;

	//variables auxiliares movimiento
	double difX;
	double difY;
	double difZ;

	double trasladaX;
	double trasladaZ;

	//variables grafo coche
	//atributos girar coche
	double factorGiro;
	double giraY;
	const double velocidad = 0.08;
	double posXcoche, posYcoche;
	int anguloOrigen = -180;

	double anguloX;
	double anguloY;
	double anguloZ;

	double rotaRuedaY;
	double rotaRuedaZ;

	const int GRADO_ROTACION = 10; //grado de rotacion al presionar x,y,z

	const int GRADO_ROTACION_RUEDAS = 5; //grado de rotacion de la rueda a los lados
	const int GRADO_ROTACION_RUEDAS_Z = 20; //grado de rotacion de la rueda sobre si misma en eje z
	const int NUM_RADIOS = 6;

	bool animacion;
	bool animacion2;

	double anguloParabrisas;
	double aperturaVentana;
	bool sentidoLimpiaParabrisas;//0 antihhorario respecto x
	bool sentidoVentana;//0 hacia abajo

	//conejo
	double giraCabeza;
	bool sentidoCabeza;

	double giraCabeza2;
	bool sentidoCabeza2;

	double giraOrejaAzul;
	bool sentidoOrejaAzul;

	double giraBase;
	bool sentidoBase;

	//FPS
	int FPS;
	bool dibujarTexto;


public:
	bool fEncendido = false;
	GLfloat* pixelSeleccionado;
	GLfloat color_flexo[3] = { 0.5,0.5,0 };
	bool luces = true;
	// Constructores por defecto y destructor
	igvEscena3D();
	~igvEscena3D();

	//metodos propios
	std::vector<igvMallaTriangulos*> getVectorMalla() { return vectorMalla; };
	void algoritmoGenerarColores();
	void algoritmoGenerarObjetos();
	void algoritmoPintarObjetos();
	// M�todos
	// m�todo con las llamadas OpenGL para visualizar la escena
	void visualizar();
	void visualizarVB();
	void aumentaX();
	void disminuyeX();
	bool get_ejes() { return ejes; };
	void set_ejes(bool _ejes) { ejes = _ejes; };
	GLdouble rotacionZFlexo = 210;
	GLdouble rotacionXFlexo = 90;
	GLdouble rotacionBrazoFlexo = 0;
	bool heRotadoZPositivo = false;
	void baseFlexo(void);
	void brazo1Flexo(void);
	void flexo(void);
	void bolaFlexo(void);
	void bolaBrazoFlexo(void);
	void aumentaZFlexo();
	void reduceZFlexo();
	void aumentaXFlexo();
	void disminuyeXFlexo();
	void aumentaXFlexoAnimacion1();
	void aumentaRotacionBrazoFlexo();
	void aumentaRotacionBrazoFlexoAnimacion1();
	void aumentaXFlexoAnimacion2();
	void aumentaRotacionBrazoFlexoAnimacion2();
	void reduceRotacionBrazoFlexo();
	void mueveFlexo(GLint x);
	bool flexoSeleccionado = false;
	GLdouble mueveFlexoZ = 0;
	GLdouble rotaCabezaConejo = 0;
	GLdouble rotaOreja1Conejo = 180;
	GLdouble rotaOreja2Conejo = -10;
	bool cambioAnimacion = false;
	bool cambioCabezaConejo = false;
	void gConejo();
	void parteConejoDos();
	void parteConejoTres();
	void parteConejoCuatro();
	void bolaCabezaConejo();
	void parteConejoCinco();
	void bolaOrejasConejo();
	void oreja1Conejo();
	void oreja2Conejo();


	void rotarCabezaConejoAbajo();
	void rotarCabezaConejoArriba();
	void rotarOreja1ConejoAbajo();
	void rotarOreja1ConejoArriba();
	void rotarOreja2ConejoAbajo();
	void rotarOreja2ConejoArriba();

	
	//igvTextura textura;


	void mueveObjeto(int objetoSeleccionado,double trasladaX,double trasladaZ);

	void desplazaObjeto(char sentido, double aumento);


	void pintar_tubo();

	double convierteDeRgb(double x);

	int cajaCoincidente(GLubyte c[]);

	//GRAFO COCHE
	void pinta_cilindro();
	void pinta_piramide();
	void pinta_cuadrado();
	void pinta_tubo();
	void coche();
	void base();
	void rueda1();
	void rueda1int();
	void rueda1int2();
	void rueda1ext();

	void rueda2();
	void rueda2int();
	void rueda2int2();
	void rueda2ext();

	void rueda1atras();


	void rueda2atras();


	void mitadCoche();
	void ventanaFrontal();
	void capoFrontal();
	void ventanaTrasera();
	void capoTrasero();

	void paloMitad();
	void paloMitad2();
	void palodelante();
	void palodelante2();
	void paloatras();
	void paloatras2();
	void techo();

	void ventana1();
	void ventana2();
	void ventana3();
	void ventana4();

	void limpiaParabrisas();
	void limpiaParabrisas2();

	void ejedelantero();
	void ejetrasero();

	void generaRadiosLlantas();

	void aumentaAngulo(char angulo, int aumento);
	int get_rotacion() { return GRADO_ROTACION; };
	double get_movimiento() { return GRADO_MOVIMIENTO; };
	int get_rotacion_ruedas() { return GRADO_ROTACION_RUEDAS; };
	int get_rotaY() { return rotaRuedaY; };
	void giraRueda(char sentido);

	bool get_animacion() { return animacion; };
	void set_animacion(bool valor) { this->animacion = valor; };

	bool get_animacion2() { return animacion2; };
	void set_animacion2(bool valor) { this->animacion2 = valor; };

	double get_anguloLimpiaparabrisas() { return anguloParabrisas; };
	void set_anguloLimpiaparabrisas(double valor) { this->anguloParabrisas = valor; };

	double get_aperturaVentana() { return aperturaVentana; };
	void set_aperturaVentana(double valor) { this->aperturaVentana = valor; };

	bool get_sentidoLimpiaParabrisas() { return sentidoLimpiaParabrisas; };
	void set_sentidoLimpiaParabrisas(bool valor) { this->sentidoLimpiaParabrisas = valor; };

	bool get_sentidoVentana() { return sentidoVentana; };
	void set_sentidoVentana(bool valor) { this->sentidoVentana = valor; };

	void setGiraY(double valor) { giraY = valor; };
	double getGiraY() { return giraY; };

	void setPosXCoche(double valor) { posXcoche = valor; };
	double getPosXCoche() { return posXcoche; };
	void setPosYCoche(double valor) { posYcoche = valor; };
	double getPosYCoche() { return posYcoche; };

	double getVelocidad() { return velocidad; };

	void setAnguloOrigen(int valor) {
		anguloOrigen = valor;
	};
	double getAnguloOrigen() { return anguloOrigen; };

	double getFactorGiro() { return factorGiro; };
	void setFactorGiro(double valor) { factorGiro = valor; };





	//GRAFO CONEJO
	void dibujaPieza15();
	void dibujaPieza16();
	void dibujaPieza17();
	void dibujaPieza18();
	void dibujaPieza18b();
	void dibujaPieza19();
	void dibujaPieza20();
	void dibujaPieza21();
	void dibujaPieza10();

	void setanguloCabeza(double valor) { giraCabeza = valor; };
	double getanguloCabeza() { return giraCabeza; };

	bool get_giraCabeza() { return sentidoCabeza; };
	void set_giraCabeza(bool valor) { this->sentidoCabeza = valor; };

	void setanguloCabeza2(double valor) { giraCabeza2 = valor; };
	double getanguloCabeza2() { return giraCabeza2; };

	bool get_giraCabeza2() { return sentidoCabeza2; };
	void set_giraCabeza2(bool valor) { this->sentidoCabeza2 = valor; };

	void setanguloOrejaAzul(double valor) { giraOrejaAzul = valor; };
	double getanguloOrejaAzul() { return giraOrejaAzul; };

	bool get_giraOrejaAzul() { return sentidoOrejaAzul; };
	void set_giraOrejaAzul(bool valor) { this->sentidoOrejaAzul = valor; };

	void setanguloBase(double valor) { giraBase= valor; };
	double getanguloBase() { return giraBase; };

	bool get_giraBase() { return sentidoBase; };
	void set_giraBase(bool valor) { this->sentidoBase = valor; };

	//FPS
	void dibujaTexto(float x, float y, float z, char* texto);

	void setFPS(int valor) { FPS = valor; };

	void algoritmoGenerarNubes();

	bool getTexto() { return dibujarTexto; };
	void setTexto(bool valor) {
		dibujarTexto = valor;
	}

	
};


#endif
